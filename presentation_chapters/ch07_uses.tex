\sectionpic{A Real-World Application of Linear Algebra}{../figures/presentation_chapters/uses.pdf}

\section{Formulating Exact Nutrient Solutions}

\begin{frame}
	\frametitle{Formulating Exact Nutrient Solutions}
	\begin{minipage}[c]{0.6\textwidth}
		\begin{figure}[H]
			\centering
			\only<1>{\includegraphics[scale=0.175]{others/plants_annotation2.png}}
			\only<2>{\includegraphics[scale=0.175]{others/plants_annotation3.png}}
		\end{figure}
	\end{minipage}%
	\begin{minipage}[c]{.4\textwidth}
		\textbf{Target concentration\only<1>{\footnote[frame]{Source: \href{http://www.e-gro.org/pdf/E303.pdf}{Neil S. Mattson}}}\only<2>{\addtocounter{footnote}{+1}\footnote[frame]{Source: \href{https://hortamericas.com/blog/news/nutrient-recipe-for-hydroponic-greenhouse-bell-peppers/}{Hort Americas}}}:}
	\begin{table}
		\begin{tabular}{lc}%
			\toprule
			\rowstyle{\bfseries}
			Element & Conc. [ppm]\\
			\midrule
			\rowcolor{col2!20}  N  & \only<1>{150}\only<2>{200}\\
			\rowcolor{col3!20}  P  & \only<1>{30}\only<2>{50}\\
			\rowcolor{col4!20}  K  & \only<1>{240}\only<2>{240}\\
			\rowcolor{white}    Ca & \only<1>{200}\only<2>{250}\\
			\rowcolor{black!10} Mg & \only<1>{45}\only<2>{50}\\
			\rowcolor{col7!20}  S  & \only<1>{70}\only<2>{155}\\
			\rowcolor{col8!20}  Cl & \only<1>{10}\only<2>{20}\\
			\bottomrule
		\end{tabular}
		\label{tab:elements}
	\end{table}
	\end{minipage}%
\end{frame}

\begin{frame}
	\frametitle{Formulating Exact Nutrient Solutions}
	\only<1>{
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.25]{others/salts_annotation.png}
		\end{figure}
	}

	\only<2->{
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.25]{others/salts_annotation_bw.png}
		\end{figure}
		\begin{tikzpicture}[overlay, remember picture]
			\node at (current page.center) {
				\begin{presentation_goal}
					Determine how much of each salt to use per 1L water to achieve the desired target concentrations.
				\end{presentation_goal}
			};
		\end{tikzpicture}
	}
\end{frame}

\section{Some Elemental Chemistry}

\begin{frame}
	\frametitle{Some Elemental Chemistry}
	\begin{presentation_reminder}
		Avogadro constant $N_{a}=6.02214076\times10^{23}$ [\si{mol^{-1}}].
	\end{presentation_reminder}
	
	\begin{table}
		\centering
		\begin{tabular}{lll}
			\toprule
			Symbol & Name (German) & Molar mass [\si{g/mol}]\\
			\midrule
			\rowcolor{col6!30} \ce{H}  & Hydrogen (Wasserstoff) & 1.008  \\
			\rowcolor{col2!30} \ce{N}  & Nitrogen (Stickstoff)  & 14.007 \\
			\rowcolor{col1!30} \ce{O}  & Oxygen (Sauerstoff)    & 15.999 \\
			\rowcolor{gray!30} \ce{Mg} & Magnesium              & 24.305 \\
			\rowcolor{col3!30} \ce{P}  & Phosphorus (Phosphor)  & 30.974 \\
			\rowcolor{col7!30} \ce{S}  & Sulfur (Schwefel)      & 32.06  \\
			\rowcolor{col8!30} \ce{Cl} & Chlorine (Chlor)       & 35.45  \\
			\rowcolor{col4!30} \ce{K}  & Potassium (Kalium)     & 39.098 \\
			\rowcolor{white}   \ce{Ca} & Calcium (Kalzium)      & 40.078 \\
			\bottomrule
		\end{tabular}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{Some Elemental Chemistry}
	\centering
	\Huge
	\ce{KH2PO4}

	\normalsize
	(Monopotassium phosphate)

	\vspace{3mm}
	\begin{tabular}{llll}
		\toprule
		Element & Molar mass & \# of Atoms & Total mass in 1 \si{mol}\\
		& [\si{gr/mol}] & in 1 unit & material [\si{gr}]\\
		\midrule
		\rowcolor{col6!30}\ce{H} & 1.008  & 2 & 2.016\\
		\rowcolor{col1!30}\ce{O} & 15.999 & 4 & 63.996\\
		\rowcolor{col3!30}\ce{P} & 30.974 & 1 & 30.974\\
		\rowcolor{col4!30}\ce{K} & 39.098 & 1 & 39.098\\
		\bottomrule
	\end{tabular}

	\vspace{3mm}
	Total molar mass: 136.084 [\si{gr/mol}].
\end{frame}

\begin{frame}
	\frametitle{Mass Fraction Vectors}
	We can create a vector representing the \textbf{mass fraction} of each element in \ce{KH2PO4}:

	\[
		\vec{m}_{f} = \frac{1000}{136.084}
		\left(\begin{array}{c}
				\rowcolor{col6!30} 2.016\\
				\rowcolor{col1!30} 63.996\\
				\rowcolor{col3!30} 30.974\\
				\rowcolor{col4!30} 39.098\\
		\end{array}\right) =
		\left(\begin{array}{c}
				\rowcolor{col6!30}  15\\
				\rowcolor{col1!30} 470\\
				\rowcolor{col3!30} 228\\
				\rowcolor{col4!30} 287\\
		\end{array}\right).
		\begin{array}{c}
			\color{col6}\leftarrow\ce{H}\\
			\color{col1}\leftarrow\ce{O}\\
			\color{col3}\leftarrow\ce{P}\\
			\color{col4}\leftarrow\ce{K}\\
		\end{array}
	\]

	\vspace{5mm}
	\begin{presentation_note}
		The mass fraction vector $\vec{m}_{f}$ is unit less.
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{Concentration Calculations}
	Let us first examine a simple problem:

	\begin{presentation_example}
		We have two salts: \KN and \AH, and we wish to make an aqueous solution with 100 \si{ppm} of nitrogen and 250 \si{ppm} of potassium (we don't care about the other elements).
	\end{presentation_example}

	\begin{presentation_note}
		Since 1 \si{Litre} of water has a mass of 1 \si{kg}, in this context \si{ppm} equals 1 \si{mg/Litre}.
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{Concentration Calculations}
	We can construct a mass fraction vector for each of the two salts, in relation to \ce{N} and \ce{K}:

	\begin{align*}
		\vec{m}_{f}^{\ce{KNO3}} &=
		\left(\begin{array}{c}
			\rowcolor{col2!30} 139\\
			\rowcolor{col4!30} 387\\
		\end{array}\right)
		\begin{array}{c}
			\color{col2}\leftarrow\ce{N}\\
			\color{col4}\leftarrow\ce{K}\\
		\end{array}\\
		\vec{m}_{f}^{\ce{NH4OH}} &=
		\left(\begin{array}{c}
			\rowcolor{col2!30} 822\\
			\rowcolor{col4!30}   0\\
		\end{array}\right)
		\begin{array}{c}
			\color{col2}\leftarrow\ce{N}\\
			\color{col4}\leftarrow\ce{K}\\
		\end{array}\\
	\end{align*}
	
	The two vectors can be compacted together to form a matrix:
	\[
		M_{f} =
		\left(\begin{array}{cc}
			\rowcolor{col2!30} 139 & 822\\
			\rowcolor{col4!30} 387 & 0\\
		\end{array}\right)
	\]
\end{frame}

\begin{frame}
	\frametitle{Concentration Calculations}
	The target concentrations can also be written as a vector:
	\[
		\vec{C} =
		\left(\begin{array}{c}
				\rowcolor{col2!30} 100\\
				\rowcolor{col4!30} 250\\
		\end{array}\right).
		\begin{array}{c}
			\color{col2}\leftarrow\ce{N}\\
			\color{col4}\leftarrow\ce{K}\\
		\end{array}
	\]
\end{frame}

\begin{frame}
	\frametitle{Concentration Calculations}
	We can then write a matrix-vector equation, with the amount of salts as the unknown:

	\[
		\left(\begin{array}{cc}
			\rowcolor{col2!30} 139 & 822\\
			\rowcolor{col4!30} 387 & 0\\
		\end{array}\right)\colvec{2}{x}{y} =
		\left(\begin{array}{c}
				\rowcolor{col2!30} 100\\
				\rowcolor{col4!30} 250\\
		\end{array}\right).
	\]

	The solution to the equation would tell us how much of each of the salts should be dissolved in 1 \si{Litre} of water to yield the desired concentrations:

	\[
		\colvec{2}{x}{y} = \colvec{2}{0.646}{0.012}.
		\begin{array}{l}
			\leftarrow\KN\\
			\leftarrow\AH
		\end{array}
	\]
\end{frame}

\begin{frame}
	\frametitle{Concentration Calculations}
	Let's validate the results: 0.646 \si{g} of \KN\ in 1 \si{Litre} water yield $0.646\times 139 = 89.79$ [\si{ppm}] nitrogen + $0.646\times 387 = 250$ [\si{ppm}] potassium. In addition, 0.012 \si{g} of \AH\ in 1 \si{Litre} water yield $0.822\times 0.012=9.87$ [\si{ppm}] of nitrogen, and no potassium.

	Together, this adds up to $89.79 + 9.87 = 99.66$ [\si{ppm}] of nitrogen and 250 [\si{ppm}] of potassium, which are almost exactly the target concentrations.
\end{frame}

\begin{frame}
	\frametitle{Tomato Fertilizer}
	Now we can use the method to calculate the amounts of salts needed to make a 1 \si{Liter} nutrient solution for tomatoes, using the table in a previous slide:

	\begin{figure}[H]
		\centering
		\includegraphics[scale=0.75]{figures/presentation_chapters/uses.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Tomato Fertilizer}
	The results are the following (per 1 \si{Litre} water):

	\begin{table}
		\centering
		\begin{tabular}{ll}
			\toprule
			Salt & Amount [\si{g}]\\
			\midrule
			\ASN & 0.589\\
			\KHP & 0.220\\
			\KOH & 0.254\\
			\CaNH & 0.374\\
			\CaO & 0.245\\
			\CaCl & 0.041\\
			\MS & 0.507\\
			\bottomrule
		\end{tabular}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{Limitations}
	Sometimes the method returns absurd results.

	\vspace{2mm}
	\begin{presentation_example}
		\begin{minipage}[t]{0.4\textwidth}
			Salts:
			\begin{itemize}
				\item \AN
				\item \KN
				\item \KS
				\item \MS
			\end{itemize}
		\end{minipage}%
		\hfill
		\begin{minipage}[t]{0.4\textwidth}
			Target concentrations:
			\begin{table}
				\centering
				\begin{tabular}{ll}
					\toprule
					Element & ppm\\
					\midrule
					\rowcolor{col2!30}\ce{N} & 150\\
					\rowcolor{col4!30}\ce{K} & 300\\
					\rowcolor{col7!30}\ce{S} & 50\\
					\rowcolor{black!30}\ce{Mg} & 50\\
					\bottomrule
				\end{tabular}
			\end{table}
		\end{minipage}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Limitations}
	\begin{presentation_example}
		Equation:
		\begin{figure}[H]
			\scalebox{0.9}{
			\begin{tikzpicture}[scale=0.9]
				\node (M) {$
					\left(\begin{array}{cccc}
						\rowcolor{col2!30}  350 & 138 &   0 &   0\\
						\rowcolor{col4!30}    0 & 387 & 449 &   0\\
						\rowcolor{col7!30}    0 &   0 & 184 & 130\\
						\rowcolor{black!30}   0 &   0 &   0 &  99\\
					\end{array}\right)
				$};
				\node[left of=M, xshift=-1.4cm] (elements) {$
					\begin{array}{c}
						\color{col2}\ce{N}\\
						\color{col4}\ce{K}\\
						\color{col7}\ce{S}\\
						\color{black!50}\ce{Mg}\\
					\end{array}
				$};
				\node[right of=M, xshift=2.9cm] (x) {$
					\colvec{4}{\AN}{\KN}{\KS}{\MS}
				$};
				\node[right of=x, xshift=0.8cm] (eq){$=$};
				\node[right of=eq, xshift=-0.1cm] (v) {$
					\left(\begin{array}{c}
						\rowcolor{col2!30}150\\
						\rowcolor{col4!30}300\\
						\rowcolor{col7!30}50\\
						\rowcolor{black!30}50\\
					\end{array}\right)
				$};

				\node[below of=M, xshift=-1cm, yshift=-2cm] (sol) {Solution: $
					\left(\begin{array}{c}
						\color{col1} 184\\
						\color{col1} 618\\
						\color{col3} -87\\
						\color{col6} 507
					\end{array}\right)\rightarrow
				$};
				\node[right of=sol, anchor=west, xshift=1cm] (soltxt) {i.e. -87 \si{mg} of \KS.};
				\node[right of=soltxt, anchor=west, xshift=1.2cm, font=\Huge] (frownie) {\frownie{}};
			\end{tikzpicture}
		}
		\end{figure}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Limitations}
	Possible solutions:
	\begin{itemize}
		\item Change target concentrations.
		\item Expand the mass-fraction matrix.
		\item Change salts.
	\end{itemize}

	\vspace{3mm}
	\begin{presentation_example}
		Substituting \ \KS\ by \ \MC\ yields the following solution:
		\[
			\left(\begin{array}{c}
				\AN\\
				\KN\\
				\MC\\
				\MS\\
			\end{array}\right) =
			\left(\begin{array}{c}
				\color{col1}224\\
				\color{col1}517\\
				\color{col2} 47\\
				\color{col6}384\\
			\end{array}\right).
		\]
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Last Note}
	Using salts that have the same mass-fraction vectors for a given set of elements causes the mass-fraction matrix to be singular (i.e. with 0 determinant), since they essentially don't add any new information to the calculation.

	\vspace{5mm}
	\begin{presentation_example}
		For the elements \ce{N}, \ce{P} and \ce{K}, the following salts all have non-zero values only in their respective \ce{K}-components:
		\[
		  \vec{m}_{f}^{\ce{KOH}} =
			\left(\begin{array}{c}
					\rowcolor{col2!30}   0\\
					\rowcolor{col3!30}   0\\
					\rowcolor{col4!30} 697\\
			\end{array}\right),\ 
		  \vec{m}_{f}^{\ce{KCl}} =
			\left(\begin{array}{c}
					\rowcolor{col2!30}   0\\
					\rowcolor{col3!30}   0\\
					\rowcolor{col4!30} 524\\
			\end{array}\right),\ 
		  \vec{m}_{f}^{\ce{KAc}} =
			\left(\begin{array}{c}
					\rowcolor{col2!30}   0\\
					\rowcolor{col3!30}   0\\
					\rowcolor{col4!30} 398\\
			\end{array}\right).
		\]
	\end{presentation_example}
\end{frame}
