\sectionpic{Sequences and Series}{../figures/presentation_chapters/sequences.pdf}

\pgfkeys{/pgfplots/Axis Style/.style={
		width=10cm, height=5cm,
		axis x line=middle,
		axis y line=left,
		xlabel={$n$},
		ylabel={$a_{n}$},
		every axis x label/.style={at={(current axis.right of origin)}, anchor=west},
		every axis y label/.style={at={(axis description cs:0,1)}, anchor=south},
		samples=200,
		xmin=0, xmax=20,
		ymin=0, ymax=1.1,
		domain=0:20,
		axis line style={thick},
		label style={font=\large},
		ticks=both,
		xticklabel style = {font=\tiny,yshift=0.5ex},
		yticklabel style = {font=\tiny,yshift=0.5ex},
		xtick={1,2,...,19},
		ytick={0.1,0.2,...,1},
}}

\section{Sequences}
\begin{frame}
	\frametitle{Definition}
	\begin{presentation_definition}
		A sequence of numbers is a set of numbers with the following additions:
		\begin{itemize}
			\item The elements are ordered (i.e. 1st element, 2nd element, etc.).
			\item Elements can repeat.
		\end{itemize}
	\end{presentation_definition}

	\begin{presentation_example}
		The following is a sequence of 11 elements:
		\[
			a = \left\{1, 3, 3, -2, 3, 0, 5, -2, 1, 1, 1\right\}
		\]
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Definition}
	We designate the elements using subscript index notation:

	\vspace{5mm}
	\begin{presentation_example}
		\[
			a_{1}=1,\ a_{2}=3,\ a_{3}=3,\ \cdots, a_{11}=1.
		\]
	\end{presentation_example}

	\onslide<2>{
		\vspace{5mm}
		\begin{presentation_note}
			Much like sets, sequences can be \textbf{finite} or \textbf{infinite.}
		\end{presentation_note}
	}
\end{frame}

\begin{frame}
	\frametitle{Formulae}
	Sequences can be defined using formulae\footnote{I don't like the word \textit{formulas}, it feels wrong.}.

	\begin{presentation_example}
		The first 3 elements of the sequence defined by $a_{n} = \frac{n^{2}}{n+3}$ are:
		\begin{align*}
			a_{1} &= \frac{1^{2}}{1+3} = \frac{1}{4},\\
			a_{2} &= \frac{2^{2}}{2+3} = \frac{4}{5},\\
			a_{3} &= \frac{3^{2}}{3+3} = \frac{9}{6} = \frac{3}{2}.\\
		\end{align*}
		\vspace{-1.3cm}
	\end{presentation_example}
\end{frame}
\begin{frame}
	\frametitle{Formulae}
	\begin{presentation_example}
		The first few elements of the sequence defined by $a_{n} = \left(\frac{1}{2}\right)^{n}$ are:
		\[
			\frac{1}{2},\ \frac{1}{4},\ \frac{1}{8},\ \frac{1}{16},\ \frac{1}{32},\ \cdots
		\]
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Sequences as Functions}
	\begin{presentation_note}
		In a sense, infinite sequences that are defined via a formula are functions of the type $\func{a}{\mathbb{N}}{\mathbb{R}}$, and can be graphed as such. For example, this is the graph of $a_{n}=\frac{1}{n}$ in the domain $n\in\left[ 1,19 \right]$:
		\begin{figure}[H]
			\centering
			\begin{tikzpicture}
				\begin{axis}[
						Axis Style,
						declare function={a(\x) = 1/\x;},
					]
					\foreach \k in {1,2,...,19}
						\addplot[col1!50!black!50, line width=1pt, mark=none, dashed] coordinates {(\k, 0) (\k, {a(\k)})};
					\addplot[col1, only marks, mark=*, samples at={1,2,...,19}] {a(x)};
				\end{axis}
			\end{tikzpicture}
		\end{figure}
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{Sequence Behavior}
	\begin{presentation_definition}
		A sequence is said to be \textbf{monotonically increasing} if for any $n\in\mathbb{N}$,
		\[
			a_{n+1} \geq a_{n}.
		\]	
	\end{presentation_definition}
	\begin{presentation_example}
		The following sequences are all monotonically increasing:
		\begin{align*}
			a_{n} &= n,\quad b_{n} = n^{2}-5,\quad c_{n} = 1,\\
			d_{n} &= -\frac{1}{2^{n}},\quad e_{n} = 2^{n},\quad f_{n} = n^{2}-n.\\
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Sequence Behavior}
	\begin{presentation_definition}
		Similarly, a sequence is said to be \textbf{monotonically decreasing} if for any $n\in\mathbb{N}$,
		\[
			a_{n+1} \leq a_{n}.
		\]	
	\end{presentation_definition}
	\begin{presentation_example}
		The following sequences are all monotonically decreasing:
		\begin{align*}
			a_{n} &= -n,\quad b_{n} = 5-n^{2},\quad c_{n} = 1,\\
			d_{n} &= \frac{1}{2^{n}}, e_{n}=2^{-n}, f_{n}=n-n^{2}.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Sequence Behavior}
	\begin{presentation_note}
		If $a_{n+1} > a_{n}$ we say the sequence is \textbf{strictly monotonically increasing}.

		\vspace{5mm}
		If $a_{n+1} < a_{n}$ we say the sequence is \textbf{strictly monotonically decreasing}.
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{Bounded Sequences}
	\begin{presentation_definition}
		Similarly, if there exists a number $\underline{M}\in\mathbb{R}$ such that for all $n\in\mathbb{N}$,
		\[
			a_{n} \geq \underline{M},
		\]
		we say that the sequence is \textbf{bounded from below} by $\underline{M}$.
	\end{presentation_definition}

	\begin{presentation_example}
		The sequence $a_{n}=3n+1$ is bounded from below by $\underline{M}=4$ (and any real number $x<\underline{M}$).
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Bounded Sequences}
	\begin{presentation_definition}
		If there exists a number $\overline{M}\in\mathbb{R}$ such that for all $n\in\mathbb{N}$,
		\[
			a_{n} \leq \overline{M},
		\]
		we say that the sequence is \textbf{bounded from above} by $\overline{M}$.
	\end{presentation_definition}

	\begin{presentation_example}
		The sequence $a_{n}=\frac{1}{n}$ is bounded from above by $\overline{M}=1$ (and any real number $x>\overline{M}$ as well).
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Bounded Sequences}
	\begin{presentation_definition}
	A sequence that is both bounded from below and from above is simply said to be \textbf{bounded}.
	\end{presentation_definition}

	\begin{presentation_example}
		The sequence $a_{n}=\sin(n)$ is bounded from below by $\underline{M}=-1$ and from above by $\overline{M}=1$.

		\vspace{2mm}
		\begin{tikzpicture}
			\begin{axis}[
					Axis Style,
					height=4cm,
					xmin=0, xmax=20,
					ymin=-1.5, ymax=1.5,
					domain=0:20,
					xtick={0,5,...,20},
					ytick={-1.5,-1,-0.5,0,0.5,1,1.5},
					declare function={s(\x) = sin(\x*180/pi);},
				]
				\foreach \k in {1,2,...,19}
					\addplot[col4!80, line width=1pt, mark=none, dashed] coordinates {(\k, 0) (\k, {s(\k)})};
				\addplot[col1, only marks, mark=*, mark size=2pt, samples at={1,2,...,19}] {s(x)};
				\addplot[col2!75!black!75, line width=1pt, dashed] {1};
				\addplot[col2!75!black!75, line width=1pt, dashed] {-1};
			\end{axis}
		\end{tikzpicture}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Bounded Sequences}
	A sequence without an \textbf{upper bound} is said to be \textbf{diverging to positive infinity}.

	We denote that the sequence $a_{n}$ is diverging to positive infinity in one of the following ways:
	\begin{itemize}
		\item $\lim\limits_{n\to\infty} a_{n}=\infty$,
		\item $a_{n} \longrightarrow \infty$.
	\end{itemize}

	We can also define the divergence of a sequence as follows:
	\begin{presentation_definition}
		A sequence is said to be diverging to positive infinity, if for \textbf{any} $M\in\mathbb{R}$ there exists $m\in\mathbb{N}$ such that for all $n>m$, $a_{n}>M$.
	\end{presentation_definition}
\end{frame}

\begin{frame}
	\frametitle{Bounded Sequences}
	Of course, the same ideas apply to sequences that diverge to negative infinity.

	\begin{presentation_example}
		The sequence $a_{n}=2n$ diverges to positive infinity.

		Proof: given a real number $M$, any integer $m>\lbceil\frac{M}{2}\rbceil$ will yield
		\[
			a_{m} = 2m > 2\cdot\lbceil\frac{M}{2}\rbceil > M.
		\]
	\end{presentation_example}
\end{frame}


\begin{frame}
	\frametitle{Subsequences}
	\begin{presentation_definition}
		Taking a sequence and removing some of its elements results in a \textbf{subsequence}.
	\end{presentation_definition}

	\begin{presentation_example}
		\begin{align*}
			a_{n} &= \left\{ \cancel{-6}, \circled{a1}{col1}{2}, \circled{a2}{col2}{3}, \cancel{0}, \cancel{1}, \circled{a3}{col3}{0}, \cancel{2}, \cancel{-2}, \cancel{5}, \cancel{7}, \circled{a4}{col4}{9}, \circled{a5}{col5}{3} \right\}\\[10mm]
			b_{n} &= \left\{ \circled{b1}{col1}{2}, \circled{b2}{col2}{3}, \circled{b3}{col3}{0}, \circled{b4}{col4}{9}, \circled{b5}{col5}{3} \right\}\\[8mm]
		\end{align*}

		\begin{tikzpicture}[overlay, remember picture]
			\draw[vector, col1, thin] (a1.south) to [out=270, in=90, looseness=0.6] (b1.north);
			\draw[vector, col2, thin] (a2.south) to [out=270, in=90, looseness=0.6] (b2.north);
			\draw[vector, col3, thin] (a3.south) to [out=270, in=90, looseness=0.6] (b3.north);
			\draw[vector, col4, thin] (a4.south) to [out=270, in=90, looseness=0.6] (b4.north);
			\draw[vector, col5, thin] (a5.south) to [out=270, in=90, looseness=0.6]  (b5.north);
		\end{tikzpicture}

		\vspace{-2cm}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Subsequences}
	\begin{presentation_example}
		\begin{itemize}
			\item The sequence $b_{n}=2n$ is a subsequence of the sequence $a_{n}=n$.
			\item The sequence $c_{n}=4n$ is a subsequence of the both $a_{n}$ and $b_{n}$.
			\item The sequence $d_{n}=3n$ is a subsequence of $a_{n}$, but \textbf{not} of $b_{n}$ nor $c_{n}$.
		\end{itemize}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Subsequences}
	If a sequence is monotonically increasing, so are any of its subsequences.

	If a sequence is monotonically decreasing, so are any of its subsequences.

	\vspace{5mm}
	\begin{presentation_challenge}
		Prove the above statements using the definitions of increasing/decreasing sequences.
	\end{presentation_challenge}
\end{frame}

\begin{frame}
	\frametitle{Subsequences}
	Similarly:

	If a sequence has no lower/upper limit, so do any of its subsequences.
	\vspace{5mm}
	\begin{presentation_challenge}
		Prove the above statements using the definitions of lower/upper limits.
	\end{presentation_challenge}
\end{frame}

\begin{frame}
	\frametitle{Limits}
	Non-formally, a limit of a sequence $a_{n}$ (if it exists) is a real number $L$ towards which the sequence approaches, but doesn't necessarily arrive.

	\begin{presentation_example}
		Intuitively, it is easy to see that the sequence $a_{n}=\frac{1}{n}$ approaches $L=0$, without ever equaling $0$:

		\vspace{-2mm}
		\begin{figure}[H]
			\centering
			\begin{tikzpicture}
				\begin{axis}[
						Axis Style,
						declare function={a(\x) = 1/\x;},
						height=4cm,
					]
					\addplot[col1, only marks, mark=*, samples at={1,2,...,19}] {a(x)};
				\end{axis}
			\end{tikzpicture}
		\end{figure}
		
		\vspace{-5mm}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Limits}
	\begin{presentation_example}
		The following sequence intuitively approaches $L=1$:

		\begin{figure}[H]
			\centering
			\begin{tikzpicture}
				\begin{axis}[
						Axis Style,
						declare function={s(\x) = sin(\x*180/pi)/exp(\x/10)+1;},
						height=4cm,
						xmin=0, xmax=50,
						ymin=0, ymax=2,
						domain=0:50,
						xtick={0,5,...,50},
						ytick={0,0.5,...,2},
						limitline/.style={black, line width=0.5pt, mark=none},
						limitstart/.style={black!40, line width=1pt, mark=none, dashed},
					]
					\tikzset{limitbox/.style={draw opacity=0, fill=col1!15}}
					\only<2>{
						\draw[limitbox] (axis cs:0,0.5) rectangle (axis cs:50,1.5);
						\addplot[limitstart] coordinates {(6,2) (6,0)};
						\addplot[limitline] {0.5};
						\addplot[limitline] {1.5};
					}
					\only<3>{
						\draw[limitbox] (axis cs:0,0.75) rectangle (axis cs:50,1.25);
						\addplot[limitstart] coordinates {(12,2) (12,0)};
						\addplot[limitline] {0.75};
						\addplot[limitline] {1.25};
					}
					\only<4>{
						\draw[limitbox] (axis cs:0,0.9) rectangle (axis cs:50,1.1);
						\addplot[limitstart] coordinates {(24,2) (24,0)};
						\addplot[limitline] {0.9};
						\addplot[limitline] {1.1};
					}
					\only<5>{
						\draw[limitbox] (axis cs:0,0.95) rectangle (axis cs:50,1.05);
						\addplot[limitstart] coordinates {(31,2) (31,0)};
						\addplot[limitline] {0.95};
						\addplot[limitline] {1.05};
					}
					\addplot[col1, only marks, mark=*, mark size=1pt, samples at={1,2,...,50}] {s(x)};
					\addplot[blue, dashed] {1};
				\end{axis}
			\end{tikzpicture}
		\end{figure}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Limits}
	\begin{presentation_definition}
		A sequence $a_{n}$ is said to be converging to the limit $L\in\mathbb{R}$ if for any real $\varepsilon>0$ there exists $\overline{n}\in\mathbb{N}$ such that for any $m>\overline{n}$,
		\[
			\left| a_{m} - L \right| < \varepsilon.
		\]

		We denote this fact as
		\[
			\lim\limits_{n\to\infty} a_{n}=L.
		\]

		(sometimes also written shortly as \ $a_{n}\overset{n\to\infty}{\longrightarrow}L$.)
	\end{presentation_definition}
\end{frame}

\begin{frame}
	\frametitle{Limits}
	\begin{presentation_example}
		The sequence $a_{n}=\frac{1}{n}$ converges to $L=0$.

		\vspace{5mm}
		\underline{Proof}: given a real number $\varepsilon>0$, any $n>\overline{n}=\lbceil\frac{1}{\varepsilon}\rbceil$ will yield
		\[
			\left| a_{n} - L \right| = a_{n} < a_{\overline{n}} < \frac{1}{\frac{1}{\varepsilon}} = \varepsilon,
		\]
		i.e. simply
		\[
			\left| a_{n} - L \right| < \varepsilon.
		\]
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Limits}
	If a sequence converges to a limit $L\in\mathbb{R}$, then \textbf{all of its subsequences} coverge as well, to the same limit.
\end{frame}

\begin{frame}
	\frametitle{Limits}
	Limits have the following arithmetic properties:
	\begin{itemize}
		\item If $\lim\limits_{n\to\infty}a_{n}=L$ then for $\alpha\in\mathbb{R}$
		\[
			\lim\limits_{n\to\infty}\left(\alpha a_{n}\right) = \alpha\left( \lim\limits_{n\to\infty}a_{n} \right).
		\]
		\item If $\lim\limits_{n\to\infty}a_{n}=L_{a}$ and $\lim\limits_{n\to\infty}b_{n}=L_{b}$ then
	\[
		\lim\limits_{n\to\infty}\left(a_{n}+b_{n}\right)=L_{a}+L_{b}.
	\]	
	\end{itemize}

	\onslide<2>{
		\begin{presentation_note}
			These two properties are very similar to the definition of linear transformations! We will learn more about this in the last chapter.
		\end{presentation_note}
	}
\end{frame}

\begin{frame}
	\frametitle{Limits}
	More arithmetic properties of limits:
	\begin{itemize}
		\setlength\itemsep{1.5em}
		\item $\lim\limits_{n\to\infty}(a_{n}b_{n})=\left(\lim\limits_{n\to\infty}a_{n}\right)\left(\lim\limits_{n\to\infty}b_{n}\right)$.
		\item $\lim\limits_{n\to\infty}\frac{a_{n}}{b_{n}} = \frac{\lim\limits_{n\to\infty}a_{n}}{\lim\limits_{n\to\infty} b_{n}}$, if $\lim\limits_{n\to\infty} b_{n} \neq 0$.
		\item $\lim\limits_{n\to\infty}\left(a_{n}\right)^{p} = \left(\lim\limits_{n\to\infty}a_{n}\right)^{p}$, for all $p>0$ and $a_{n}>0$.
	\end{itemize}
\end{frame}

\section{Series}
\begin{frame}
	\frametitle{Definition}
	\begin{presentation_definition}
		A \textbf{series} is the sum of a sequence.
	\end{presentation_definition}

	\begin{presentation_example}
		Given the sequence $a_{n} = \left\{ 1, -3, 4, 4, 9, -2, 0, 5\right\}$, a series can be constructed as sums of the form $a_{1}+a_{2}+\cdots+a_{n}$:
		\begin{align*}
			S_{1} &= 1,\\
			S_{2} &= 1+(-3),\\
			S_{3} &= 1+(-3)+4,\\
			&\vdots\\
			S_{8} &= 1-3+4+4+9-2+0+5 = 18.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Partial Sums}
	Since sequences can be infinite, so do their sums.

	\begin{presentation_example}
		Given the sequence $a_{n}=\frac{1}{2^{n}}$, we can derive the \textbf{partial sum} $S_{n}$:
		\begin{align*}
			S_{n} &= a_{1} + a_{2} + a_{3} + \cdots + a_{n}\\
			&= \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \cdots + \frac{1}{n}\\
			&= \sum\limits_{k=1}^{n}\frac{1}{2^{k}}.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Infinite Sums}
	\begin{presentation_definition}
		Using the summation notation we define
		\[
			\sum\limits_{i=1}^{\infty} a_{i} = \lim\limits_{n\to\infty}\sum\limits_{i=1}^{n}a_{i}.
		\]
	\end{presentation_definition}
	
	\begin{presentation_example}
		The infinite sum of the previous sequence will therefore be
		\begin{align*}
			S &= \sum\limits_{k=1}^{\infty}\frac{1}{2^{k}} = \lim\limits_{n\to\infty}\sum\limits_{k=1}^{n}\frac{1}{2^{k}} = \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \cdots\\
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Convergent Series}
	\begin{presentation_definition}
		If the following is true:
		\[
			S = \sum\limits_{k=1}^{\infty}a_{n} = \lim\limits_{n\to\infty}\sum\limits_{k=1}^{n}a_{k} = L\in\mathbb{R},
		\]
		we say that the series $S$ of partial sums $\left\{S_{n}\right\}$ \textbf{converges} to the limit $L\in\mathbb{R}$.
	\end{presentation_definition}

	\begin{presentation_note}
		In order for a series $S=\sum\limits_{k=1}^{\infty}a_{n}$ to converge, the elements $a_{n}$ must converge to $L=0$. The inverse statement however is not true: $a_{n}\to 0\ \centernot\implies\ S=\sum\limits_{k=1}^{\infty}a_{n}\to L$.
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{Convergent Series}
	\begin{presentation_example}
		The sequence $a_{n}=e^{-n}$ converges to $L=0$, and also $S=\sum\limits_{k=1}^{\infty}a_{n}$ converges to $L=\frac{1}{e-1}\approx0.58198$.

		However, if we add even a tiny amount $\varepsilon>0$ to the sequence $a_{n}$, i.e. $a_{n}=e^{-n}+\varepsilon$, then $a_{n}\to\varepsilon$, but $S$ goes to $\infty$.
	\end{presentation_example}

	\begin{presentation_example}
		The sequence $a_{n}=\frac{1}{n}$ converges to $L=0$, however
		\[
			S=\sum\limits_{k=1}^{\infty}\frac{1}{n}\to\infty.
		\]
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Convergence Tests for Infinite Series}
	There are tests we can employ to check whether an infinite series converges or not. We'll list some of them.
	\begin{presentation_definition}
		The \textbf{ratio-test} (also \textbf{d'Alembert's criterion}):
		If there exists a real number $r$ such that
		\[
			\lim\limits_{n\to\infty}\left|\frac{a_{n+1}}{a_{n}}\right|=r,
		\]
		then:
		\begin{itemize}
			\item if $r<1$ the series converges.
			\item if $r>1$ the series diverges.
			\item if $r=1$ the series might converge or diverge.
		\end{itemize}
	\end{presentation_definition}
\end{frame}

\begin{frame}
	\frametitle{Convergence Test for Infinite Series}
	\begin{presentation_example}
		Let $a_{n}=\frac{1}{2^{n}}$. Then
		\[
			\frac{a_{n+1}}{a_{n}} = \frac{\frac{1}{2^{n+1}}}{\frac{1}{2^{n}}} = \frac{1}{2}<1,
		\]
		and therefore $\lim\limits_{n\to\infty}\left|\frac{a_{n+1}}{a_{n}}\right| = \frac{1}{2} < 1$, and the series converges.
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Convergence Tests for Infinite Series}
	\begin{presentation_definition}
		$\bm{p}$\textbf{-series test}: $\sum\limits_{n>0}^{\infty}\left( \frac{1}{n^{p}} \right)$ converges if $p>1$.
	\end{presentation_definition}

	\begin{presentation_example}
		The infinite series $S=\sum\limits_{n=1}^{\infty}\frac{1}{n}$ ($p=1$) diverges to infinity. However:

		\vspace{5mm}
		\begin{tabular}{lll}
			$\sum\limits_{k=1}^{\infty}\frac{1}{n^{1.0001}} \approx 1000,$ & $\sum\limits_{k=1}^{\infty}\frac{1}{n^{1.01}} \approx 100$,& $\sum\limits_{k=1}^{\infty}\frac{1}{n^{2}} = \frac{\pi^{2}}{6}$,\\[3mm]
			
			$\sum\limits_{k=1}^{\infty}\frac{1}{n^{3}} \approx 1.2,$ & $\sum\limits_{k=1}^{\infty}\frac{1}{n^{4}} \approx 1.08$,& $\sum\limits_{k=1}^{\infty}\frac{1}{n^{5}} = 1.037$.\\
		\end{tabular}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Convergence Tests for Infinite Series}
	\begin{presentation_definition}
		\textbf{Direct comparison test}: if $\sum\limits_{n=1}^{\infty}\left|a_{n}\right|=L$, and $\left|a_{n}\right| > \left|b_{n}\right|$ for any $n>n_{0}$ (where $n_{0}\in\mathbb{N}$), then the series $\sum\limits_{n=1}^{\infty}\left|b_{n}\right|$ converges as well.
	\end{presentation_definition}

	\begin{presentation_example}
		For any $n>0\ (n\in\mathbb{N}),\ 0 < e^{-n} < \frac{1}{n^{2}}$. Since we know that $\sum\limits_{n=1}^{\infty}\left|\frac{1}{n^{2}}\right|$ converges, so does $\sum\limits_{n=1}^{\infty}e^{-n}$ (remember that for $n>0$ both $\frac{1}{2^{n}}=\left|\frac{1}{2^{n}}\right|$ and $e^{-n}=\left|e^{-n}\right|$).
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Common Types of Infinite Series}
	Some common types of infinite series:

	\begin{presentation_definition}
		A \textbf{geometric series} is a series of the form
		\[
			\sum\limits_{n=0}^{\infty}ar^{n},
		\]
		where $a\in\mathbb{R}$ is the \textbf{coefficient} of each term and $r\in\mathbb{R}$ is the \textbf{common ratio} between two consecutive terms.
	\end{presentation_definition}
\end{frame}

\begin{frame}
	\frametitle{Geometric Series}
	\begin{presentation_example}
		The series
		\begin{align*}
			\sum\limits_{n=0}^{\infty}\frac{1}{2^{n}} &= \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \cdots\\
			&= \frac{1}{2}\cdot\left(\frac{1}{2}\right)^{0} + \frac{1}{2}\cdot\left(\frac{1}{2}\right)^{1} + \frac{1}{2}\cdot\left(\frac{1}{2}\right)^{2} \cdots\\
		\end{align*}
		is a geometric series with $a=r=\frac{1}{2}$.
	\end{presentation_example}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Geometric Series}
	Some simple geometric series:

	\pyc{from scripts.geo_series import *}
	\renewcommand{\arraystretch}{1.2}
	\begin{table}
	\centering
		\begin{tabular}{ccl}
			\toprule
			$a$ & $r$ & First few elements\\
			\midrule
			\pyc{series_example(a=1, r=2)}\\
			\pyc{series_example(a=3, r=1)}\\
			\pyc{series_example(a=3, r=-1)}\\
			\pyc{series_example(a=0.5, r=0.5)}\\
			\pyc{series_example(a=0.5, r=-0.5)}\\
			\pyc{series_example(a=1, r=-0.3333333333)}\\
			\pyc{series_example(a=2, r=10, n_max=5)}\\
			\pyc{series_example(a=2, r=0.1, n_max=5)}\\
			\bottomrule
		\end{tabular}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{Geometric Series}
	The sum of the first $n$ terms in a geometric series is
	\[
		s = a\frac{1-r^{n+1}}{1-r}.
	\]
	If $|r|<1$ the infinite geometric series converges to the limit
	\[
		S = \frac{a}{1-r}.
	\]

	\begin{presentation_example}
		The geometric sequence $\sum\limits_{n=0}^{\infty}\left(-\frac{1}{3}\right)^{n}$ converges to the limit
		\[
			s = \frac{1}{1+\frac{1}{3}} = \frac{1}{\frac{4}{3}} = \frac{3}{4}.
		\]
	\end{presentation_example}
\end{frame}
