\sectionpic{Limits of Real Functions}{../figures/presentation_chapters/function_limits.pdf}

\pgfmathsetmacro{\rd}{180/pi}
\pgfkeys{/pgfplots/Axis Style/.style={
		width=7cm, height=7cm,
		axis x line=middle,
		axis y line=center,
		xlabel={$x$},
		ylabel={$y$},
		every axis x label/.style={at={(current axis.right of origin)}, anchor=west},
		every axis y label/.style={at={(axis description cs:0.5,1)}, anchor=south},
		samples=200,
		xmin=-3, xmax=3,
		ymin=-3, ymax=3,
		domain=-3:3,
		axis line style={thick, stealth-stealth},
		label style={font=\large},
		ticks=none,
		xticklabel style={font=\tiny,yshift=0.5ex},
		yticklabel style={font=\tiny,yshift=0.5ex},
		grid=both,
		major grid style={line width=.4pt, draw=black!15},
		minor grid style={line width=.2pt, draw=black!10},
    minor tick num=4,
}}

\section{Limits at \texorpdfstring{$\bm{\pm\infty}$}{pminfty}}
\begin{frame}
	\frametitle{Limits as \texorpdfstring{$\bm{x\to\pm\infty}$}{pminfty}}
	Much like sequences, $\mathbb{R}\to\mathbb{R}$ functions can behave in one of three ways as $x\to\infty$:

	\begin{itemize}
		\item $f(x)\to\pm\infty$,
		\item $f(x)\to L\in\mathbb{R}$, or
		\item The function has no limit as $x\to\infty$.
	\end{itemize}

	Also like with sequences, we denote this behavior as follows:
	\[
		\xlim{x}{\infty} f(x).
	\]
\end{frame}

\begin{frame}
	\frametitle{Limits as \texorpdfstring{$\bm{x\to\pm\infty}$}{pminfty}}
	\begin{presentation_example}
		For the following function $f_{\only<1>{1}\only<2>{2}\only<3>{3}\only<4>{4}}$, as $x\to\infty$, $f_{\only<1>{1}\only<2>{2}\only<3>{3}\only<4>{4}}(x)\only<1-3>{\to}\only<1>{+\infty}\only<2>{-\infty}\only<3>{0}$\only<4>{\ has no limit}:
		\begin{figure}[H]
			\centering
			\begin{tikzpicture}
				\begin{axis}[
						Axis Style,
						declare function={f1(\x)=exp(x)*(\x^3-0.5*\x^2-\x);},
						declare function={f2(\x)=-exp((x-7)/2)*(4*\x^2-\x)+2;},
						declare function={f3(\x)=2*exp(-(x+1)^2/1.7);},
						declare function={f4(\x)=cos(3*\x*\rd)*sin(6*\x*\rd);},
					]
					\only<1>{\addplot[function, col1] {f1(x)};}
					\only<2>{\addplot[function, col2] {f2(x)};}
					\only<3>{\addplot[function, col3] {f3(x)};}
					\only<4>{\addplot[function, col4] {f4(x)};}
				\end{axis}
			\end{tikzpicture}
		\end{figure}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Limits as \texorpdfstring{$\bm{x\to\pm\infty}$}{pminfty}}
	\begin{presentation_definition}
		A function $\func{f}{\mathbb{R}}{\mathbb{R}}$ is said to be \textbf{diverging to infinity as } $\bm{x\to\infty}$, if for any real number $\overline{M}\in\mathbb{R}$ there exists $x_{0}\in\mathbb{R}$ such that for any $x>x_{0}$, $f(x)>\overline{M}$.
	\end{presentation_definition}

	\vspace{5mm}
	\begin{presentation_note}
		Replacing $\overline{M}$ by $\underline{M}$ and setting the condition to $f(x)<\underline{M}$, yields a divergence to $-\infty$.
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{Limits as \texorpdfstring{$\bm{x\to\pm\infty}$}{pminfty}}
	Convergence to a real number $L$ is also defined in a similar fashion to convergence of sequences:

	\vspace{2mm}
	\begin{presentation_definition}
		A function $\func{f}{\mathbb{R}}{\mathbb{R}}$ is said to be \textbf{converging to a real number (limit)} $\bm{L}$ \textbf{at infinity}, if for any real $\varepsilon>0$ there exists a real number $x_{0}$ such that for any $x>x_{0}$, $\left| f(x)-L \right| < \varepsilon$.
	\end{presentation_definition}
\end{frame}

\begin{frame}
	\frametitle{limits as \texorpdfstring{$\bm{x\to\pm\infty}$}{pminfty}}
	lastly, if a function does not converge to a limit nor diverges to $\pm\infty$ as $x\to\infty$, we say that the function simply \textbf{diverges} as $x\to\infty$.

	\vspace{5mm}
	\begin{presentation_note}
		of course, replacing the condition $\forall x>x_{0}$ with $\forall x<x_{0}$ yields the respective behavior as $x\to-\infty$.
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{limits as \texorpdfstring{$\bm{x\to\pm\infty}$}{pminfty}}
	\begin{presentation_example}
		The function $f(x)=x^{2}$ diverges to $\infty$ as $x\to\infty$:

		For any real $\overline{M}$, we can choose $x_{0}=\sqrt{\overline{M}}+1$, which guarantees that for any $x>x_{0}$,
		\begin{align*}
			f(x) &> f(x_{0})\\
			&= f\left( \sqrt{\overline{M}}+1 \right)\\
			&> f\left(\sqrt{\overline{M}}\right)\\
			&= \left(\sqrt{\overline{M}}\right)^{2}\\
			&= \overline{M}.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{limits as \texorpdfstring{$\bm{x\to\pm\infty}$}{pminfty}}
	\begin{presentation_example}
		The function $f(x)=-e^{x}$ diverges to $-\infty$ as $x\to\infty$:

		For any real $\underline{M}$, we can choose $x_{0}=-\log\left(\underline{M}\right)$, which guarantees that for any $x>x_{0}$,
		\begin{align*}
			f(x) &< f(x_{0})\\
			&= -e^{x_{0}}\\
			&= -e^{-\log\left(\underline{M}\right)}\\
			&= \underline{M}.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{limits as \texorpdfstring{$\bm{x\to\pm\infty}$}{pminfty}}
	\begin{presentation_example}
		The function $f(x)=e^{-x}$ converges to $L=0$ as $x\to\infty$:

		For any real $\varepsilon>0$ we can choose $x_{0}=-\log\left( \varepsilon \right)$ which guarantees that for any $x>x_{0}$,
		\begin{align*}
			\left|f(x)-0\right| &= \left|f(x)\right| \tikzmark{eq}=\ f(x)\\
			&= e^{-x}\\
			&< e^{-x_{0}}\\
			&= e^{\log\left( \varepsilon \right)}\\
			&= \varepsilon.
		\end{align*}
		\begin{tikzpicture}[overlay, remember picture, node distance=1cm]
			\node[above right of=eq, xshift=2cm, col1] (eqtxt) {(since $e^{-x}>0$)};
			\draw[vector, thin, col1] (eqtxt.west) to [out=180, in=90, looseness=1.0] ($(eq.north)+(2.5mm,1mm)$);
		\end{tikzpicture}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Limits Arithmetics}
	As with limits of infinite sequences, limits of $\mathbb{R}\to\mathbb{R}$ functions have the following properties:
	\begin{itemize}
		\item $\xlim{x}{\pm\infty}\left[f(x)\pm g(x)\right] = \xlim{x}{\pm\infty}f(x) \pm \xlim{x}{\pm\infty}g(x)$.
		\item $\xlim{x}{\pm\infty}\left[f(x) \cdot g(x)\right] = \xlim{x}{\pm\infty}f(x) \cdot \xlim{x}{\pm\infty}g(x)$.
		\item $\xlim{x}{\pm\infty}\left[ \frac{f(x)}{g(x)} \right] = \frac{\xlim{x}{\pm\infty}f(x)}{\xlim{x}{\pm\infty}g(x)}$ (if $g(x)\xrightarrow{x\to\pm\infty}0$ things get a bit more complicated).
		\item $\xlim{x}{\pm\infty}\left[ f(x)^{g(x)}\right] = \left[\xlim{x}{\pm\infty}f(x)\right]^{\xlim{x}{\pm\infty}g(x)}$.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Limits Arithmetics}
	\begin{presentation_example}
		\begin{align*}
			\xlim{x}{\infty}\left(e^{-x} + \frac{1}{1+e^{-x}}\right) &= \xlim{x}{\infty}e^{-x} + \frac{\xlim{x}{\infty}1}{\lim\limits_{x\to\infty}1 + \xlim{x}{\infty}e^{-x}}\\
			&= 0 + \frac{1}{1+0}\\
			&= 1.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Limits of Polynomials}
	The limits of polynomial functions as $x\to\pm\infty$ are dictated by their order $n$ and the respective coefficient $a_{n}$. The following table summarizes the possible behaviors:

	\begin{minipage}[c]{0.45\textwidth}
		\begin{table}
			\centering
			\begin{tabular}{lccc}
				\toprule
				Parity of & sign of & \multicolumn{2}{c}{Limit at}\\
				$n$ & $a_{n}$ & $-\infty$ & $+\infty$\\
				\midrule
				\rowcolor<1>{col1!50}Even & $+$ & $+\infty$ & $+\infty$\\
				\rowcolor<2>{col2!50}Even & $-$ & $-\infty$ & $-\infty$\\
				\rowcolor<3>{col3!50}Odd  & $+$ & $-\infty$ & $+\infty$\\
				\rowcolor<4>{col4!50}Odd  & $-$ & $+\infty$ & $-\infty$\\
				\bottomrule
			\end{tabular}
		\end{table}
	\end{minipage}%
	\hfill
	\begin{minipage}[c]{0.45\textwidth}
		\begin{figure}[H]
						\centering
						\begin{tikzpicture}
							\begin{axis}[
								Axis Style,
								width=6cm, height=6cm,
								grid=major,
								ticks=both,
								xtick={-2,...,2},
								ytick={-2,...,2},
								minor x tick num=0,
								minor y tick num=0,
								xticklabels={,,},
								yticklabels={,,},
								]
								\only<1>{\addplot[function, col1] {0.5*x^2};}
								\only<2>{\addplot[function, col2] {-0.5*x^2};}
								\only<3>{\addplot[function, col3] {0.5*x^3};}
								\only<4>{\addplot[function, col4] {-1*x^3+2*x^2};}
							\end{axis}
						\end{tikzpicture}
					\end{figure}
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Limits of Rational Functions}
	\begin{presentation_definition}
		A \textbf{Rational Function} is a function composed of a ratio of two Polynomials, i.e.
		\[
			f(x) = \frac{P_{n}(x)}{P_{m}(x)}.
		\]
	\end{presentation_definition}
	\begin{presentation_example}
		The following is a rational functions:
		\[
			f(x) = \frac{2x^{3}-3x+5}{-4x^{4}+5x^{2}-7}.
		\]
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Limits of Rational Functions}
	Similarly to polynomials, the limits of rational functions are also determined by the highest order element. However, If that element is in the denominator, the function will converge to $0$ at $\pm\infty$.

	We can see this by dividing both the numerator and the denominator by $x^{n}$, where $n$ is the highest power in both.

	\begin{presentation_example}
		\begin{align*}
			f(x) &= \frac{\frac{2x^{3}-3x+5}{x^{4}}}{\frac{-4x^{4}+5x^{2}-7}{x^{4}}} = \frac{\frac{2x^{3}}{x^{4}}-\frac{3x}{x^{4}}+\frac{5}{x^{4}}}{-\frac{4x^{4}}{x^{4}}+\frac{5x^{2}}{x^{4}}-\frac{7}{x^{4}}}\\
			&= \frac{\frac{2}{x}-\frac{3}{x^{3}}+\frac{5}{x^{4}}}{-4+\frac{5}{x^{2}}-\frac{7}{x^{4}}}\xrightarrow[]{x\to\pm\infty}\frac{0-0+0}{-4+0-0} = 0.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Limits of Rational Functions}
	\begin{presentation_example}
		\begin{align*}
			\xlim{x}{\infty} \frac{x^{4}-5}{100x^{3}-2x^{2}+x} &= \xlim{x}{\infty} \frac{\frac{x^{4}}{x^{4}}-\frac{5}{x^{4}}}{100\frac{x^{3}}{x^{4}}-2\frac{x^{2}}{x^{4}}+\frac{x}{x^{4}}}\\
			&=\xlim{x}{\infty} \frac{1-\frac{5}{x^{4}}}{\frac{100}{x}-\frac{2}{x^{2}}+\frac{1}{x^{3}}}\\
			"&="\frac{1-0}{0-0+0}\\
			"&="\infty.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Limits of Rational Functions}
	When the numerator and the denominator are polynomials of the same order, the limit at $\pm\infty$ is determined by the ratio of their leading coefficients.
	\begin{presentation_example}
		\[
			\renewcommand\arraystretch{2}
			\begin{array}{l}
				\xlim{x}{\infty} \frac{2x^{3}-3x^{2}+5}{3x^{3}-1} = \frac{2}{3}.\\
				\xlim{x}{-\infty} \frac{-2x^{5}+7}{4x^{5}+2x+9} = -\frac{2}{4}.\\
				\xlim{x}{\infty} \frac{5x-4}{x+1} = \frac{5}{1} = 5.\\
			\end{array}
		\]
	\end{presentation_example}
\end{frame}

\section{Limit at a Point}
\begin{frame}[label={exfunc}]
	\pgfmathsetmacro{\xx}{2}
	\pgfmathsetmacro{\xmax}{5}
	\pgfmathsetmacro{\ymax}{5}
	\frametitle{Limit at a Point}
	\begin{figure}[H]
		\centering
		\begin{tikzpicture}
			\begin{axis}[
					Axis Style,
					xmin=-0.6, xmax=\xmax,
					ymin=-0.4, ymax=\ymax,
					domain=-0.2:10,
					every axis y label/.style={at={(axis description cs:0,1)}, anchor=south},
					declare function={f(\x)=4*exp(\x)/(1+exp(\x));},
					xtick = {\xx},
					ytick = {f(\xx)},
					ticks=both,
					xticklabel style={font=\normalsize, xshift=0ex},
					yticklabel style={font=\normalsize, yshift=0ex},
					xticklabel={$a$},
					yticklabel={$L$},
				]
				\addplot[col1, thick] {f(\x)};
				\addplot[thick, col1, fill=white, only marks, mark size=1pt, mark=*, samples at={\xx}] {f(\x)};
				\tikzset{
					limitbox/.style={thick, fill opacity=0.1},
					xlimitbox/.style={limitbox, draw=col2, fill=col2},
					ylimitbox/.style={limitbox, draw=col3, fill=col3}
				}
				\only<2>{
					\pgfmathsetmacro{\dx}{1}
					\pgfmathsetmacro{\dy}{0.6}
					\draw[xlimitbox] (axis cs:0,{f(\xx)-\dy}) rectangle (axis cs:6,{f(\xx)+\dy});
					\draw[ylimitbox] (axis cs:{\xx-\dx},0) rectangle (axis cs:{\xx+\dx},6);
					\tikzset{arrowtype/.style={stealth-stealth}}
					\draw[arrowtype] (axis cs:{\xx-\dx}, 0.25) -- (axis cs:{\xx}, 0.25) node[midway, above] {$-\delta$};
					\draw[arrowtype] (axis cs:{\xx}, 0.25) -- (axis cs:{\xx+\dx}, 0.25) node[midway, above] {$+\delta$};
					\draw[arrowtype] (axis cs:0.25, {f(\xx)-\dy}) -- (axis cs:0.25, {f(\xx)}) node[midway, right] {$-\varepsilon$};
					\draw[arrowtype] (axis cs:0.25, {f(\xx)}) -- (axis cs:0.25, {f(\xx)+\dy}) node[midway, right] {$+\varepsilon$};
				}
				\only<3>{
					\pgfmathsetmacro{\dx}{0.5}
					\pgfmathsetmacro{\dy}{0.28}
					\draw[xlimitbox] (axis cs:0,{f(\xx)-\dy}) rectangle (axis cs:6,{f(\xx)+\dy});
					\draw[ylimitbox] (axis cs:{\xx-\dx},0) rectangle (axis cs:{\xx+\dx},6);
				}
				\only<4>{
					\pgfmathsetmacro{\dx}{0.2}
					\pgfmathsetmacro{\dy}{0.12}
					\draw[xlimitbox] (axis cs:0,{f(\xx)-\dy}) rectangle (axis cs:6,{f(\xx)+\dy});
					\draw[ylimitbox] (axis cs:{\xx-\dx},0) rectangle (axis cs:{\xx+\dx},6);
				}
				\only<5>{
					\pgfmathsetmacro{\dx}{0.1}
					\pgfmathsetmacro{\dy}{0.075}
					\draw[xlimitbox] (axis cs:0,{f(\xx)-\dy}) rectangle (axis cs:6,{f(\xx)+\dy});
					\draw[ylimitbox] (axis cs:{\xx-\dx},0) rectangle (axis cs:{\xx+\dx},6);
				}
			\end{axis}
		\end{tikzpicture}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Limit at a Point: Definition}
	\begin{presentation_definition}
		A function $f$ has a limit $L\in\mathbb{R}$ at a point $x_{0}$, denoted as
		\[
			\lim\limits_{x\to a} f(x) = L,
		\]
		if for any $\varepsilon>0$ there exist $\delta>0$ such that if
		\[
			0 < |x-a| < \delta,
		\]
		then
		\[
			|f(x)-L| < \varepsilon.
		\]
	\end{presentation_definition}
\end{frame}

\begin{frame}
	\frametitle{Limit at a Point}
	\begin{presentation_example}
		Let's show that $\xlim{x}{0} x^{2}=0$:

		For any given $\varepsilon>0$ we choose $\delta=\sqrt{\varepsilon}$. We can then write the inequality $0 < |x-\cancelto{0}{a}| < \delta$ as
		\[
			0 < |x| < \sqrt{\varepsilon}.
		\]
		For any such $x$, $\left|f(x)-L\right| = \left|f(x)\right| = \left|x^{2}\right| = x^{2} < \delta^{2} = \left( \sqrt{\varepsilon} \right)^{2} = \varepsilon$, i.e.
		\[
			|f(x)-0| < \varepsilon,
		\]
		proving the stated limit.
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Note}
	\begin{presentation_note}
		The limit of a function $f$ at a point $a$ \textbf{DOES NOT} depend on the value $f(a)$! In fact, this point can be removed from the function, and the limit would stay the same (example: look at the function in slide \ref{exfunc}).
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{Directional Limits}
	We can use similar ideas to define limits from the positive and negative directions of $x$.
	\begin{figure}[H]
		\centering
		\pgfmathsetmacro{\xx}{-0.4}
		\pgfmathsetmacro{\dx}{0.1}
		\begin{tikzpicture}[
				pointat/.style={-stealth, ultra thick, cap=round},
				limits/.style={font=\small, thick, fill=white, rounded corners},
			]
			\begin{axis}[
					Axis Style,
					xmin=-1, xmax=1,
					ymin=-1, ymax=1,
					declare function={f(\x)={-1.2*(\x^3-0.95*\x^2)*exp(-\x^2/2)};},
				]
				\coordinate (fx) at (axis cs:\xx, {f(\xx)});
				\addplot[function, col2!50] {f(\x)};
				\addplot[thick, col2!50, fill=white, only marks, mark size=1pt, mark=*, samples at={\xx}] {f(\x)};
				\draw[pointat, col1] (axis cs:{\xx-\dx)},{f(\xx-\dx)}) to (axis cs:{\xx},{f(\xx)});
				\draw[pointat, col4] (axis cs:{\xx+\dx)},{f(\xx+\dx)}) to (axis cs:{\xx},{f(\xx)});
			\end{axis}
			\node[limits, text=col1, draw=col1] at (0.4,3.5) {$\lim\limits_{x\to a^{-}}f(x)$};
			\node[limits, text=col4, draw=col4] at (2.85,3.4) {$\lim\limits_{x\to a^{+}}f(x)$};
		\end{tikzpicture}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Directional Limits}
	\begin{presentation_example}
		\begin{minipage}[t]{0.45\textwidth}
			The function
			\[
				f(x) =
				\begin{cases}
					x^{2} & x<1\\
					-x    & x>1\\
				\end{cases}
			\]
			has two different directional limits at $x=1$:
			\begin{align*}
				\xlim{x}{1^{-}}f(x) &= 1\\
				\xlim{x}{1^{+}}f(x) &= -1.
			\end{align*}
			(and no value at $x=1$!)
		\end{minipage}%
		\hfill
		\begin{minipage}[t]{0.45\textwidth}
			\begin{figure}[H]
				\centering
				\begin{tikzpicture}
					\begin{axis}[
						Axis Style,
						width=6cm, height=6cm,
						grid=major,
						ticks=both,
						xtick={-2,...,2},
						ytick={-2,...,2},
						minor x tick num=0,
						]
						\addplot[function, col1, domain={-3:1}] {x^2};
						\addplot[function, col1, domain={1:3}] {-x};
						\addplot[thick, col1, fill=white, only marks, mark size=1pt, mark=*, samples at={1}] {-1};
						\addplot[thick, col1, fill=white, only marks, mark size=1pt, mark=*, samples at={1}] {1};
						\draw[col1, densely dotted] (axis cs:1,-1) -- (axis cs:1,1);
					\end{axis}
				\end{tikzpicture}
			\end{figure}
		\end{minipage}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Directional Limits}
	\begin{presentation_definition}
		$\lim\limits_{x\to a} f(x) = L$ \textbf{if and only if}
		$\lim\limits_{x\to a^{-}} f(x) = \lim\limits_{x\to a^{+}} f(x) = L$.
	\end{presentation_definition}
\end{frame}

\begin{frame}
	\frametitle{\texorpdfstring{$\bm{\pm\infty}$ Limit at a Point}{pminfty}}
	Functions can also go to $\pm\infty$ at specific points.
	\begin{presentation_example}
		The function $f(x)=\frac{x^{2}}{x^{2}-1}$ has the following limits:

		\begin{minipage}[c]{0.45\textwidth}
			\begin{itemize}
				\item $\xlim{x}{-\infty}f(x) = 1$.
				\item $\xlim{x}{-1^{-}}f(x) = \infty$.
				\item $\xlim{x}{-1^{+}}f(x) = -\infty$.
				\item $\xlim{x}{1^{-}}f(x) = -\infty$.
				\item $\xlim{x}{1^{+}}f(x) = \infty$.
				\item $\xlim{x}{\infty}f(x) = 1$.
			\end{itemize}
		\end{minipage}%
		\hfill
		\begin{minipage}[c]{0.45\textwidth}
			\begin{figure}[H]
				\centering
				\begin{tikzpicture}
					\begin{axis}[
							Axis Style,
							width=6cm, height=6cm,
							grid=major,
							ticks=major,
							xmin=-5, xmax=5,
							xtick={-5,...,5},
							ytick={-3,...,3},
							vasymptote={-1},
							vasymptote={1},
							domain={-5:5},
							restrict y to domain=-5:5,
						]
						\addplot[function, col1, domain={-5:-1}] {x^2/(x^2-1)};
						\addplot[function, col1, domain={-1:1}] {x^2/(x^2-1)};
						\addplot[function, col1, domain={1:5}] {x^2/(x^2-1)};
						\addplot[black!75, densely dashed] {1};
					\end{axis}
				\end{tikzpicture}
			\end{figure}
		\end{minipage}
	\end{presentation_example}
\end{frame}

\section{Continuity}
\begin{frame}
	\frametitle{Continuity of a Function at a Point}
	\begin{presentation_definition}
		We say that a function $f$ is \textbf{continuous} at a point $x_{0}$ if
		\[
			\lim\limits_{\tikzmark{a}x\to x_{0}\tikzmark{b}} f(x)=f(x_{0}).
		\]

		\begin{tikzpicture}[overlay, remember picture]
			\coordinate (c) at ($(a)!0.5!(b)$);
			\node[below left of=c, font=\tiny, col1] (ctxt) {(from both sides!)};
			\draw[vector, thin, col1] (ctxt.north) to [out=90, in=-90] ($(c.south)-(0,1mm)$);
		\end{tikzpicture}
	\end{presentation_definition}
\end{frame}

\begin{frame}
	\frametitle{Continuity of a Function at a Point}
	\begin{presentation_example}
		The function $f(x)=x$ is continuous at any point, specifically $x_{0}=2$. However, we can multiply it by $\frac{x-2}{x-2}$, yielding
		\[
			g(x) = f(x)\frac{x-2}{x-2} = \frac{x(x-2)}{x-2} = \frac{x^{2}-2x}{x-2}
		\]

		For any $x\neq2$, $g(x)=f(x)$. However, $g(x)$ is clearly undefined at $x_{0}=2$, and thus
		\[
			\xlim{x}{2}g(x) \neq g(2),
		\]
		and the function is said to be continuous for any $x\neq2$.
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Continuity of a Function at a Point}
	\begin{presentation_example}
		\begin{figure}[H]
			\centering
			\begin{tikzpicture}
				\begin{axis}[
						Axis Style,
						ticks=both,
						domain=-5:5,
						xmin=-5, xmax=5,
						xtick={-5,...,5},
						ymin=-5, ymax=5,
						ytick={-5,...,5},
						minor tick num=1,
						declare function={g(\x)=\x;},
					]
					\addplot[function, col1] {g(x)};
					\node[label={180:{$(2,2)$}}, circle, thick, draw=col1, fill=white, inner sep=1.5pt] at (axis cs:2,2) {};
				\end{axis}
			\end{tikzpicture}
		\end{figure}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Continuity of a Function on an Interval}
	\begin{presentation_definition}
		A function is said to be \textbf{continuous on an interval}\ $\bf{I}$ if it is continuous at any point $x\in I$.
	\end{presentation_definition}
\end{frame}

\begin{frame}
	\frametitle{Continuity of a Function on an Interval}
	\begin{presentation_example}
		The following function is continuous on the intervals
		\[
			(-\infty, -1.5),\ (-1.5,2.5),\ (2.5, \infty).
		\]

		\vspace{-5mm}
		\begin{figure}[H]
			\centering
			\begin{tikzpicture}
				\begin{axis}[
						Axis Style,
						width=6cm, height=6cm,
						ticks=both,
						domain=-5:5,
						xmin=-5, xmax=5,
						xtick={-5,...,5},
						ymin=-5, ymax=5,
						ytick={-5,...,5},
						minor tick num=1,
						declare function={
							f1(\x)=sin(3*\x*\rd);
							f2(\x)=-exp(-\x);
							f3(\x)=\x;
						}
					]
					\addplot[function, col3, domain={-5:-1.5}] {f1(x)};
					\addplot[function, col3, domain={-1.5:2.5}] {f2(x)};
					\addplot[function, col3, domain={2.5:5}] {f3(x)};

					\addplot[function, col3!50!black!50, densely dotted] coordinates {(-1.5, {f1(-1.5)}) (-1.5, {f2(-1.5)})};
					\addplot[function, col3!50!black!50, densely dotted] coordinates {(2.5, {f2(2.5)}) (2.5, {f3(2.5)})};
				\end{axis}
			\end{tikzpicture}
		\end{figure}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Common (Mostly) Continuous Functions}
	Following is a list of some common functions and intervals on which they are continuous:

	\begin{minipage}[c]{0.45\textwidth}
		\begin{table}
			\centering
			\begin{tabular}{ll}
				\toprule
				Function & Continuity Interval(s)\\
				\midrule
				\rowcolor<1>{col1!50}$P_{n}(x)$ & $\mathbb{R}$\\
				\rowcolor<2>{col2!50}$e^{x}$    & $\mathbb{R}$\\
				\rowcolor<3>{col3!50}$\log(x)$  & $(0,\infty)$\\
				\rowcolor<4>{col4!50}$\sin(x)$  & $\mathbb{R}$\\
				\rowcolor<5>{col5!50}$\cos(x)$  & $\mathbb{R}$\\
				\rowcolor<6>{col6!50}$\tan(x)$  & $x\notin \frac{(2k+1)\pi}{2},\ k\in\mathbb{Z}$\\
				\bottomrule
			\end{tabular}
		\end{table}
	\end{minipage}%
	\hfill
	\begin{minipage}[c]{0.45\textwidth}
		\begin{figure}[H]
			\centering
			\begin{tikzpicture}
				\begin{axis}[
					Axis Style,
					width=6cm, height=6cm,
					grid=major,
					ticks=both,
					xmin=-11, xmax=11,
					ymin=-6, ymax=6,
					xtick={-10,-5,5,10},
					ytick={-5,-2.5,2.5,5},
					minor x tick num=0,
					domain={-11:10},
					restrict y to domain={-10:10},
					]
					\only<1>{\addplot[function, col1] {0.04*x^4-0.7*x^2};}
					\only<2>{\addplot[function, col2] {exp(x)};}
					\only<3>{\addplot[function, col3] {ln(x)};}
					\only<4>{\addplot[function, col4] {sin(180/pi*x)};}
					\only<5>{\addplot[function, col5] {cos(180/pi*x)};}
					\only<6>{\addplot[function, col6, samples=500] {tan(180/pi*x)};}
				\end{axis}
			\end{tikzpicture}
		\end{figure}
	\end{minipage}
\end{frame}
