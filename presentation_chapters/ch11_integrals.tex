\sectionpic{Integrals}{../figures/presentation_chapters/integrals.pdf}

\pgfkeys{/pgfplots/Axis Style/.style={
		width=9cm, height=7cm,
		axis x line=middle,
		axis y line=left,
		every axis x label/.style={
			at={(ticklabel* cs:1.05)},
			anchor=west,
		},
		every axis y label/.style={
			at={(ticklabel* cs:1.05)},
			anchor=south,
		},
		axis line style={-stealth, thick},
		label style={font=\large},
		tick label style={font=\small},
		hide y axis,
		samples=200,
		xmin=-5, xmax=5,
		ymin=0, ymax=5,
		domain=-5:5,
}}
\tikzset{
	asymp/.style={black, densely dashed},
	intrec/.style={thin, fill=#1, fill opacity=0.2},
}
\pgfplotsset{
	intfpoint/.style={fpoint, mark size=1pt},
}

\newcommand\darboux[1]{
	\pgfmathsetmacro{\N}{#1}
	\pgfmathsetmacro{\M}{\N-1}
	\pgfmathsetmacro{\dx}{(\b-\a)/\N}
	\addplot[intfpoint, samples at={\a,\a+\dx,...,\b}] {f(x)};
	\foreach \i in {0,...,\M}{
		\edef\darbouxMax{
			\noexpand\draw[intrec={col2}] (axis cs:{\a+\i*\dx},0) rectangle (axis cs:{\a+(\i+1)*\dx},{max(f(\a+\i*\dx),f(\a+(\i+1)*\dx))});
		}
		\darbouxMax
	}
}

\begin{frame}
	\frametitle{Definite and Indefinite Integrals}
	Two different concepts that are called \textbf{integrals}:

	\begin{figure}[H]
		\centering
		\begin{tikzpicture}[node distance=3cm]
			\tikzset{concept/.style={fill=####1!20, draw=####1!75, thick, rounded corners, text width=3.5cm, align=center}}
			\node[concept={col1}] (def) {Area under a curve (on some interval): \textbf{definite integral}};
			\node[concept={col2}, right of=def, xshift=3cm] (indef) {Antiderivative: \textbf{indefinite integral}};
			\coordinate (mid) at ($(def)!0.5!(indef)$);
			\node[concept={col4}, below of=mid] (fund) {Fundamental theorem of calculus};
			\draw[vector, col1, thick] (def.south) -- (fund.north west);
			\draw[vector, col2, thick] (indef.south) -- (fund.north east);
		\end{tikzpicture}
	\end{figure}
\end{frame}

\section{Definite Integral (Area)}
\begin{frame}
	\frametitle{Definite Integral}
	\begin{minipage}[t][2cm][c]{\textwidth}
		\only<1>{
			What is the area $S$ underneath the curve of a continuous function $f$ and the $x$-axis, in the interval $\left[ a,b \right]$?
		}
		\only<2>{
			We can approximate the area by a rectangle of width $\Delta x=b-a$ and height of the maximum between $f(a)$ and $f(b)$. This gives the estimation
			\[
				S \approx \max\left( f(a), f(b) \right)\cdot\Delta x = S_{1}.
			\]
		}
		\only<3>{
			Of course, we can do better: we split the interval $\left[ a,b \right]$ into two equal parts, such that now $\Delta x=\frac{b-a}{2}$, and create two rectangles, each with an area $\max\left( f(x_{k}), f(x_{k+1}) \right)\cdot\Delta x$, where $x_{1}=a,\ x_{2}=\frac{b-a}{2},\ x_{3}=b$ are the $x$-coordinates of the rectangle's sides.
		}
		\only<4-6>{
			We can continue to split interval into $N=3,4,5,\dots$ rectangles, each time setting $\Delta x=\frac{b-a}{N}$. The total approximate area for each $N$ would be
			\vspace{-7mm}
			\[
				S_{N} = \sum\limits_{k=1}^{N}\max\left(f(x_{k}), f(x_{k+1}) \right)\cdot\Delta x.
			\]
		}
		\only<7>{
			We see that when $N\to\infty$ (which is equivalent to $\Delta x\to 0$), $S_{N} \to S$, i.e.
			\[
				S = \xlim{N}{\infty}S_{N} = \xlim{\Delta x}{0} S_{N}.
			\]
		}
	\end{minipage}
	\begin{minipage}[b][5.5cm][b]{\textwidth}
		\begin{figure}[H]
			\centering
			\begin{tikzpicture}
				\pgfmathsetmacro{\a}{-0.3}
				\pgfmathsetmacro{\b}{0.45}
				\pgfmathsetmacro{\xmin}{-1}
				\pgfmathsetmacro{\xmax}{1}
				\begin{axis}[
						Axis Style,
						samples=500,
						domain={\xmin:\xmax},
						xtick={\a, \b},
						xticklabels={$a$, $b$},
						xmin=\xmin, xmax=\xmax,
						ymin=0, ymax=2,
						restrict y to domain=0:2,
						declare function={f(\x)=exp(-\x^2/0.2)+1/(1+exp(\x));},
					]
					\addplot[name path=f, function, col1] {f(x)};
					\path[name path=xaxis] (axis cs:-1,0) -- (axis cs:1,0);
					\addplot[intfpoint, samples at={\a, \b}] {f(x)};
					\draw[asymp] (axis cs:\a,0) -- (axis cs:\a,{f(\a)}) node[left] {$f(a)$};
					\draw[asymp] (axis cs:\b,0) -- (axis cs:\b,{f(\b)}) node[right] {$f(b)$};
					\only<1>{
						\addplot[col4, opacity=0.3] fill between [of=f and xaxis, soft clip={domain=\a:\b}];
						\node at (axis cs:{(\a+\b)/2}, {f((\a+\b)/2)/2}) {$S$};
					}
					\only<2>{
						\darboux{1}
						\draw[thick, stealth-stealth] (axis cs:\a,0.1) -- (axis cs:{\a+\dx},0.1) node[midway, above] {$\Delta x$};
					}
					\only<3>{
						\darboux{2}
						\draw[thick, stealth-stealth] (axis cs:\a,0.1) -- (axis cs:{\a+\dx},0.1) node[midway, above] {$\Delta x$};
						\draw[thick, stealth-stealth] (axis cs:{\a+\dx},0.1) -- (axis cs:{\b},0.1) node[midway, above] {$\Delta x$};
					}
					\only<4>{
						\darboux{5}
					}
					\only<5>{
						\darboux{10}
					}
					\only<6>{
						\darboux{15}
					}
					\only<7>{
						\darboux{25}
					}
				\end{axis}
			\end{tikzpicture}
		\end{figure}
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Definite Integral}
	We call the sums in the sequence $S_{N}$ \textbf{upper Darboux sums}. If we change the $\max$ function in the definition to $\min$, we get the respective \textbf{lower Darboux sums}.

	When $\Delta x\to 0$ the values $x_{k}$ and $x_{k+1}$ approach each other. In addition, as with derivatives, when $\Delta x\to 0$ we replace it with $\dif x$. We thus get the following notation:
	\[
		S = \sum\limits_{k=1}^{N}\max\left(f(x_{k}), f(x_{k+1}) \right)\cdot\Delta x = \int\limits_{a}^{b}\!f(x)\dif x.
	\]
\end{frame}

\section{Indefinite Integral (Antiderivative)}
\begin{frame}
	\frametitle{Antiderivitive}
	As we saw in the chapter on derivatives, we can define higher order derivatives as following:

	\begin{align*}
		f'(x) &= \od{}{x}f(x),\\
		f''(x) &= \od{}{x}\left[\od{}{x}f(x)\right] = \od[2]{}{x}f(x),\\
		f'''(x) &= \od{}{x}\left[\od[2]{}{x}f(x)\right] = \od[3]{}{x}f(x),\\
		&\vdots\\
		f^{(n)}(x) &= \od[n]{}{x}f(x).
	\end{align*}
\end{frame}

\begin{frame}
	\frametitle{Antiderivative}
	We can use the same logic to define an \textbf{antiderivative}: $F$ is the antiderivative of $f$ (with respect to $x$) if
	\[
		F(x) = \od[-1]{}{x}f(x),
	\]
	i.e.
	\[
		f(x) = \od{}{x}F(x).
	\]
\end{frame}

\begin{frame}
	\frametitle{Antiderivative}
	\begin{presentation_example}
		An antiderivative of the function
		\[
			f(x)=3x^{2}+5
		\]
		is, for example,
		\[
			F(x) = x^{3}+5x,
		\]
		as $F'(x)=f(x)$.
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Antiderivative}
	Since the derivative operator is linear and constants are derived to $0$, addition of any constant term to a function does not affect its derivative:
	\[
		[f(x) + c]' = f'(x) + \cancel{c'} = f'(x).\quad (c\in\mathbb{R})
	\]

	Therefore, the antiderivative of a function is in fact a set of functions differing only in a constant term:
	\[
		\od[-1]{}{x}f(x) = \left\{F(x)+c\right\}, 
	\]
	for all $c\in\mathbb{R}$.
\end{frame}

\begin{frame}
	\frametitle{Antiderivative}
	\begin{presentation_example}
	In the following examples $c\in\mathbb{R}$.
		\begin{align*}
			\od[-1]{}{x}\left[ 2x + e^{x}  \right] &= x^{2}+e^{x}+c,\\
			\od[-1]{}{x}\left[ \cos(x)\right] &= \sin(x)+c,\\
			\od[-1]{}{x}\left[ e^{x}\left( 1+x \right)\right] &= xe^{x}+c.\\
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Antiderivative}
	The antiderivative of a function $f(x)$ is most commonly denoted as
	\[
		\int\! f(x)\dif x.
	\]
	(the symbol $\dif x$ signifies that the antiderivation in respect to the variable $x$)
	
	This is very similar to the notation of the definite integral of the function, except without an interval.

	Therefore, together with the fact that the result is a set of functions, this is called an \textbf{indefinite integral}.
\end{frame}

\begin{frame}
	\frametitle{Antiderivative}
	\begin{presentation_example}
		Some more indefinite integrals:

		\vspace{5mm}
		\centering
		\begin{tabular}[H]{lll}
			\underline{$F(x)$} & \multirow{9}{*}{\tikz{\draw[vector, ultra thick, col1] (0,0) -- (2.2,0) node[midway, above, font=\bfseries] {$\od{}{x}F(x)$};
			\draw[vector, ultra thick, col2] (2.2,-0.5) -- (0,-0.5) node[midway, below, font=\bfseries] {$\displaystyle\int\! f(x) \dif x$};}} & \underline{$f(x)$}\\[1mm]
			$c$ & & $0$\\
			$x+c$ & & $1$\\
			$x^{2}+c$ & & $2x$\\
			$x^{n}+c$ & & $nx^{n-1}$\\
			$\frac{x^{n+1}}{n+1}+c$ & & $x^{n}$\\
			$e^{x}+c$ & & $e^{x}$\\
			$\sin(x)+c$ & & $-\cos(x)$\\
			$\cos(x)+c$ & & $\sin(x)$\\
		\end{tabular}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Special Methods of For Finding Antiderivatives}
	Not every function $f$ has an antiderivative.

	Even if the antiderivative exists, it is not straight-forward to find it.

	We will now examine two methods that can be used to find antiderivatives:
	\begin{enumerate}
		\item Integration by parts.
		\item Integration by substitution.
	\end{enumerate}
\end{frame}

\begin{frame}
	\frametitle{Integration by Parts}
	For any two functions $f$, $g$ we know that following:
	\[
		(fg)' = f'g + fg'.
	\]

	Integrating both sides of the equation yields
	\[
		\underbrace{\int\! (fg)' \dif x}_{fg} = \int\! f'g \dif x + \int\! fg' \dif x.
	\]

	Rearranging the above gives us
	\[
		\int\! fg' \dif x = fg - \int\! f'g \dif x.
	\]
\end{frame}

\begin{frame}
	\frametitle{Integration by Parts}
	\begin{presentation_example}
		Let's find the antiderivative of $u(x)=x\cos(2x)$:

		If we define $f(x)=x$ and $g'(x)=\cos(2x)$, we get
		\[
			g(x) = \int\! g'(x)\dif x = \int\! \cos(2x)\dif x = \frac{1}{2}\sin(2x).
		\]

		Using
		\[
			\int\! fg' \dif x = fg - \int\! f'g \dif x,
		\]
		we get
		\begin{align*}
			\int\! x\cos(2x)\dif x &= \frac{1}{2}x\sin(2x) - \frac{1}{2}\int\!\sin(2x)\dif x\\
			&= \frac{1}{2}\left[ x\sin(2x) + \frac{1}{2}\cos(2x)  \right] + C.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Integration by Parts}
	\begin{presentation_example}
		Verifying the result:
		\begin{align*}
			\left(\frac{1}{2}\left[ x\sin(2x)+\frac{1}{2}\cos(2x) \right]\right)' &= \frac{1}{2}\left[ \sin(2x)+2x\cos(2x)\right.\\
			&- \left.\cancel{2}\frac{1}{\cancel{2}}\sin(2x) \right]\\
			&= \frac{1}{2}\left[ \cancel{\sin(2x)}+2x\cos(2x)\right.\\
			&- \left.\cancel{\sin(2x)} \right]\\
			&= x\cos(2x).
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Integration by Substitution}
	Another method that can be used is \textbf{integration by substitution}, which we shall demostrate via an example:

	\begin{presentation_example}
		Let us evaluate $\displaystyle\int\! \frac{x}{\sqrt{1+x^{2}}}\dif x$.

		We can set a new function $u(x)=1+x^{2}$. Deriving this new function yields
		\[
			\od{u}{x} = 2x,
		\]
		and thus $\dif u = 2x\dif x$, or
		\[
			x\dif x = \frac{1}{2}\dif u.
		\]
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Integration by Substitution}
	\begin{presentation_example}
		Substituting the expression $x\dif x$ in the integral by $\frac{1}{2}u$, and $1+x^{2}$ by $u$ yields
		\[
			\int\!\frac{x}{\sqrt{1+x^{2}}}\dif x = \int\! \frac{1}{2\sqrt{u}}\dif u = \frac{1}{2}\int\! u^{-\frac{1}{2}}\dif u.
		\]
		The last integral is easy to calculate:
		\[
			\frac{1}{2}\int\! u^{-\frac{1}{2}}\dif u = \frac{1}{\cancel{2}}\cdot\cancel{2}u^{\frac{1}{2}} + C = \sqrt{u} + C.
		\]

		Substituting $u(x)=1+x^{2}$ back into the last expression gives
		\[
			\int\! \frac{x}{\sqrt{1+x^{2}}}\dif x = \sqrt{1+x^{2}} + C.
		\]
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Integration by Substitution}
	\begin{presentation_example}
		As always, we verify the answer by derivation:

		\[
			\left[\sqrt{1+x^{2}}\right]' = \frac{\left[1+x^{2}\right]'}{2\sqrt{1+x^{2}}} = \frac{\cancel{2}x}{\cancel{2}\sqrt{1+x^{2}}} = \frac{x}{\sqrt{1+x^{2}}}.
		\]
	\end{presentation_example}

	\begin{presentation_note}
		Recall that that $\left[ \sqrt{f(x)} \right]' = \frac{f'(x)}{2\sqrt{f(x)}}$.
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{Integration by Substitution}
	Another example:

	\begin{presentation_example}
		Let's evaluate $\displaystyle\int\! xe^{-x^{2}}\dif x$:

		We can define $u = -x^{2}$, which gives us
		\[
			\od{u}{x} = -2x\ \Rightarrow \dif u = -2x\dif x,
		\]
		i.e.
		\[
			x\dif x = -\frac{1}{2}\dif u.
		\]

		Therefore,
		\[
			\int\! xe^{-x^{2}}\dif x = -\frac{1}{2}\int\! e^{u}\dif u = -\frac{1}{2}e^{u} + C = -\frac{1}{2}e^{-x^{2}} + C.
		\]
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Integration by Substitution}
	\begin{presentation_example}
		Validating the result:

		\[
			\left[ -\frac{1}{2}e^{-x^{2}}+C \right]' = -\frac{1}{\cancel{2}}\left[ -\cancel{2}xe^{-x^{2}} \right] = xe^{-x^{2}}.
		\]
	\end{presentation_example}
\end{frame}

\section{The Fundamental Theorem of Calculus}
\begin{frame}
	\frametitle{The Fundamental Theorem of Calculus}
	\textbf{The fundamental theorem of calculus} binds together the two concepts we discussed so far: areas and anti-derivatives. It can be phrased as following:
	Given a function $\func{f}{\mathbb{R}}{\mathbb{R}}$ on the interval $\left[ a,b \right]$, and $\func{F}{\mathbb{R}}{\mathbb{R}}$ such that $\od{}{x}F(x)=f(x)$, then
	\[
		\int\limits_{a}^{b}\!f(x)\dif x = F(b) - F(a).
	\]
\end{frame}

\begin{frame}
	\frametitle{The Fundamental Theorem of Calculus}
	\begin{presentation_example}
		Let us calculate the area between the function
		\[
			f(x)=e^{x}
		\]
		and the $x$-axis, on the interval $\left[ 0,1 \right]$.

		Graphically, this means the following area in green:
		\begin{figure}[H]
			\centering
			\begin{tikzpicture}
				\begin{axis}[
					Axis Style,
					width=9cm, height=4cm,
					xmin=-1, xmax=2,
					xtick={-1,-0.5,...,1.5},
					ymin=-1, ymax=4,
					axis y line=middle,
					hide y axis=false,
					ytick={2},
					declare function={g(\x)=exp(\x);},
				]
					\addplot[name path=gfunc, function, col1] {g(x)};
					\path[name path=xaxis] (axis cs:-1,0) -- (axis cs:2,0);
					\addplot[col3, opacity=0.3] fill between [of=gfunc and xaxis, soft clip={domain=0:1}];
					\draw[thick, densely dashed, col4!75!black] (axis cs:1,0) -- (axis cs:1,{g(1)});
				\end{axis}
			\end{tikzpicture}
		\end{figure}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{The Fundamental Theorem of Calculus}
	\begin{presentation_example}
		The fundamental theorem of calculus tells us that this area will equal
		\[
			F(1)-F(0),
		\]
		where $F(x)=\bigint e^{x}\dif x$. Calcululating the antiderivative of $e^{x}$ is pretty easy: it's simply $e^{x}+C$. Thus:
		\begin{align*}
			F(1) - F(0) &= e^{1}+\cancel{C} - \left(e^{0}+\cancel{C}\right)\\
			&= e^{1}-e^{0}\\
			&= e-1\\
			&\approx 1.71828\dots
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{The Fundamental Theorem of Calculus}
	\begin{presentation_note}
		When using the fundamental theorem of calculus we always add and subtract the same constant $C$, and therefore from now on we will simply ignore it.
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{The Fundamental Theorem of Calculus}
	\begin{presentation_example}
		Let us calculate the \textbf{signed} area between the curve of
		\[
			f(x)=\cos(x)
		\]
		and the $x$-axis on the interval $\left[ \frac{\pi}{2},\frac{5\pi}{2} \right]$:
		\begin{figure}[H]
			\centering
			\begin{tikzpicture}
				\begin{axis}[
					Axis Style,
					width=9cm, height=4cm,
					xmin=-1, xmax=10,
					xtick={1.5707963267948966, 4.71238898038469, 7.853981633974483},
					xticklabels={$\frac{\pi}{2}$, $\frac{3\pi}{2}$, $\frac{5\pi}{2}$},
					xticklabel style={
						yshift=-3ex,
						name=xlabel,
						append after command={(xlabel.north) edge [thin, densely dashed] ++(0,3ex)},
					},
					ymin=-2, ymax=2,
					axis y line=middle,
					hide y axis=false,
					domain={-1:10},
					declare function={g(\x)=cos(180/pi*\x);},
				]
					\addplot[name path=gfunc, function, col1] {g(x)};
					\path[name path=xaxis] (axis cs:-1,0) -- (axis cs:10,0);
					\addplot[col3, opacity=0.3] fill between [of=gfunc and xaxis, soft clip={domain=pi/2:2.5*pi}];
				\end{axis}
			\end{tikzpicture}
		\end{figure}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{The Fundamental Theorem of Calculus}
	\begin{presentation_example}
		We should calculate $F\left( \frac{5\pi}{2} \right)-F\left( \frac{\pi}{2} \right)$, where
		\[
			F(x)=\int\! \cos(x) \dif x.
		\]

		Since $\od{}{x}\sin(x)=\cos(x)$ we get that
		\begin{align*}
			\int\limits_{\frac{\pi}{2}}^{\frac{5\pi}\!{2}}\cos(x)\dif x &= \sin(x)\Biggr|_{\frac{\pi}{2}}^{\frac{5\pi}{2}} = \sin\left( \frac{5\pi}{2} \right) - \sin\left( \frac{\pi}{2} \right)\\
			&= 1-1 = 0.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Symmetric and Antisymmetric Functions}
	\begin{presentation_definition}
		A \textbf{symmetric function} is a function $\func{f}{\mathbb{R}}{\mathbb{R}}$ for which
		\[
			f(-x) = f(x).
		\]
		An \textbf{antisymmetric function} is a function $\func{g}{\mathbb{R}}{\mathbb{R}}$ for which
		\[
			g(-x) = -g(x).
		\]
	\end{presentation_definition}
\end{frame}

\begin{frame}
	\frametitle{Symmetric and Antisymmetric Functions}
	\begin{presentation_note}
		For any symmetric function $f$,
		\[
			\int\limits_{-a}^{a}\!f(x)\dif x = 2\int\limits_{0}^{a}\!f(x)\dif x.
		\]

		For any antisymmetric function $g$,
		\[
			\int\limits_{-a}^{a}\!g(x)\dif x = 0.
		\]
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{Symmetric and Antisymmetric Functions}
	\begin{presentation_example}
		\begin{align*}
			\int\limits_{-3}^{3}\! x^{2}\dif x &= 2\int\limits_{0}^{3}\! x^{2}\dif x = 2\frac{1}{3}x^{3}\Biggr|_{0}^{3} = \frac{2}{3}\cdot3^{3}=18.\\
			\int\limits_{-3}^{3}\! x^{3}\dif x &= \frac{x^{4}}{4}\Bigg|_{-3}^{3} = \frac{1}{4}\left[ (-3)^4 - 3^{4} \right] = 81-81 = 0.
		\end{align*}
	\end{presentation_example}
\end{frame}
