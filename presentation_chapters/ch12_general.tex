\sectionpic{$\mathbb{R}\to\mathbb{R}$ Functions as Vector Spaces}{../figures/presentation_chapters/general.pdf}

%\begin{frame}
%	\frametitle{Square-integrable Functions}
%	We first introduce a new set:
%	\begin{presentation_definition}
%		Functions $f$ of type $\func{f}{\mathbb{R}}{\mathbb{C}}$ for which
%		\[
%			\int\limits_{a}^{b}\left|f(x)\right|^{2}\dif x < \infty
%		\]
%		where $a,b\in\mathbb{R}$, or
%		\[
%			\int\limits_{-\infty}^{\infty}\left|f(x)\right|^{2}\dif x < \infty,
%		\]
%		are called \textbf{square-integrable functions}. The set of all real square-integrable functions is denoted as $\cinf$.
%	\end{presentation_definition}
%\end{frame}

\newcommand{\real}[1]{#1:\mathbb{R}\to\mathbb{R}}
\newcommand{\freal}{\real{f}}
\newcommand{\cinf}{C^{1}[a,b]}
\newcommand{\BRn}{\pmb{\mathbb{R}}^{\bm{n}}}
\newcommand{\bk}[2]{\langle #1 \mid #2 \rangle}
\renewcommand{\dist}{\operatorname{dist}}

\begin{frame}
	\frametitle{Notations Used}
	\begin{presentation_note}
		We denote the set of all continuous functions $\freal$ which are continuously differentiable on $[a,b]$ as $\cinf$.
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{Properties of $\Rs{n}$ and $\cinf$}
	Let's look at some properties of vectors in $\Rs{n}$ (some of these might seem pretty obvious), and note that they also apply to $\cinf$ functions:
	\begin{table}
		\centering
		\begin{tabular}{lll}
			\toprule
			Property & Vectors in $\Rs{n}$ & Functions in $\cinf$\\
			\midrule
			Closure & $\vec{v}+\vec{u}\in\Rs{n}$ & $f(x)+g(x)\in \cinf$\\
			Commutativity & $\vec{v}+\vec{u}=\vec{u}+\vec{v}$ & $\left[ f+g \right](x)=f(x)+g(x)$\\
			Associativity & $\begin{aligned}[t]&\vec{v}+(\vec{u}+\vec{w})=\\&(\vec{v}+\vec{u})+\vec{w}\end{aligned}$ & $\begin{aligned}[t]&f(x)+[g+h](x)=\\&[f+g](x)+h(x)\end{aligned}$\\
			Identity & $\vec{v}+\vec{0}=\vec{v}$ & $\left[f+0\right](x)=f(x)$\\
			Inverse & $\vec{v}+(-\vec{v})=\vec{0}$ & $f(x)+[-f](x)=0$\\
			\bottomrule
		\end{tabular}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{Properties of $\Rs{n}$ and $\cinf$}
	\begin{table}
		\centering
		\begin{tabular}{lll}
			\toprule
			Property & Vectors in $\Rs{n}$ & Functions in $\cinf$\\
			\midrule
			Closure & $\alpha\vec{v}\in\Rs{n}$ & $\alpha f(x)\in\cinf$\\
			Compatibility & $\alpha(\beta\vec{v})=(\alpha\beta)\vec{v}$ & $\alpha[\beta f](x)=(\alpha\beta) f(x)$\\
		Scalar identity & $1\cdot\vec{v}=\vec{v}$ & $\left[1\cdot f\right](x) = f(x)$\\
			\multirow{2}{*}{Distributivity }& $\alpha\cdot (\vec{u}+\vec{v})=\alpha\vec{u}+\alpha\vec{v}$ & $\begin{aligned}[t]&\alpha\cdot[f+g](x) =\\ &\alpha f(x) + \alpha g(x)\end{aligned}$\\
			& $(\alpha+\beta)\cdot \vec{v} = \alpha\vec{v}+\beta\vec{v}$ & $\begin{aligned}[t]&(\alpha+\beta)\cdot f(x)=\\&\alpha f(x) + \beta f(x)\end{aligned}$\\
			\bottomrule
		\end{tabular}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{\texorpdfstring{$\bm{\cinf}$}{cinf} as a Vector Space}
	We see that functions in $\cinf$ behave, in a way, like vectors.

	More formally we say that the functions in $\cinf$, together with real numbers as scalars and the addition operator, form a \textbf{vector space over the real numbers} (the latter is known in this context as a \textbf{field}).

	We will now analyze this behaviour and the consequences that arise from it.
\end{frame}

\begin{frame}
	\frametitle{Dimension of \texorpdfstring{$\bm{\cinf}$}{sqrint}}
	What is the dimension of $\cinf$?

	It's pretty easy to understand what is the dimension of a space $\Rs{n}$: $\Rs{2}$ is 2-dimensional, $\Rs{3}$ is 3-dimensional, etc.

	A more general definition of the dimension of a vector space is:

	\vspace{3mm}
	\begin{presentation_definition}
		The dimension of a vector space $V$, denoted $\dim(V)$, is the number of elements in any of its \textbf{basis sets}.
	\end{presentation_definition}
\end{frame}

\begin{frame}
	\frametitle{Dimension of \texorpdfstring{$\bm{\cinf}$}{sqrint}}
	\begin{presentation_example}
		All basis sets of $\Rs{3}$ have 3 vectors in them, e.g.
		\[
			\left\{ \colvec{3}{1}{0}{0}\ \colvec{3}{0}{1}{0} \colvec{3}{0}{0}{1} \right\},\quad
			\left\{ \colvec{3}{1}{3}{-1}\ \colvec{3}{0}{1}{1} \colvec{3}{2}{5}{0} \right\},\quad\dots
		\]
		
		Therefore, $\dim\left( \Rs{3} \right)=3$.
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{The Dirac Delta}
	To form a basis set for $\cinf$, we will define a new function, called the \textbf{Dirac delta function}, denoted by $\delta(x)$.

	We first define the following function:

	\begin{minipage}[c]{0.5\textwidth}
		\begin{presentation_definition}
			\vspace{-5mm}
			\[
				\delta_{\varepsilon}(x) =
				\begin{cases}
					\frac{1}{2\varepsilon} & x\in\left[ -\varepsilon, \varepsilon \right],\\
					0 & x\notin\left[ -\varepsilon, \varepsilon \right].
				\end{cases}
			\]
		\end{presentation_definition}

		\vspace{3mm}
		\begin{presentation_note}
			\vspace{-5mm}
			\[
				\int\limits_{-\infty}^{\infty}\delta_{\varepsilon}(x)\dif x = \cancel{2\varepsilon} \cdot \frac{1}{\cancel{2\varepsilon}} = 1.
			\]
		\end{presentation_note}
	\end{minipage}%
	\hfill
	\begin{minipage}[c]{0.4\textwidth}
		\begin{figure}[H]
			\centering
			\begin{tikzpicture}
				\begin{axis}[
						Axis Style,
						width=6cm, height=6cm,
						xmin=-2.5, xmax=2.5,
						ymin=0, ymax=1.5,
						ticks=both,
						xtick={-1,1},
						xticklabels={$-\varepsilon$, $\varepsilon$},
						y axis line style={-stealth},
						ytick={1},
						yticklabels={$\frac{1}{2\varepsilon}$},
						yticklabel style={left, xshift=-1cm},
					]
					\addplot+[const plot, no marks, ultra thick, col4, fill=col4, fill opacity=0.15] coordinates {(-2.4,0) (-1,0) (-1,1) (1,1) (1,0) (2.4,0)} ;
				\end{axis}
			\end{tikzpicture}
		\end{figure}
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{The Dirac Delta}
	We can then define the \textbf{Dirac delta function} as follows:

	\begin{minipage}[c]{0.5\textwidth}
		\begin{presentation_definition}
			\vspace{-5mm}
		\[
			\delta(x) = \lim\limits_{\varepsilon\to 0} \delta_{\varepsilon}(x).
		\]
		\end{presentation_definition}
	\end{minipage}%
	\hfill
	\begin{minipage}[c]{0.4\textwidth}
		\begin{figure}[H]
			\centering
			\begin{tikzpicture}
				\begin{axis}[
						Axis Style,
						width=6cm, height=4cm,
						xmin=-2.5, xmax=2.5,
						ymin=0, ymax=1.5,
						ticks=both,
						axis y line=none,
						xtick={0},
					]
					\addplot+[vector, const plot, no marks, very thick, col4] coordinates {(0,0) (0,1)} ;
					\addplot[col4, fill=white, very thick, only marks, mark=*, samples at={0}] {0};
				\end{axis}
			\end{tikzpicture}
		\end{figure}
	\end{minipage}

	\vspace{3mm}
	\begin{presentation_note}
		Since the area under the function $\delta_{\varepsilon}(x)$ doesn't depend on $\varepsilon$, we say that the same applies to the Dirac delta, i.e.

		\[
			\int\limits_{-\infty}^{\infty}\delta(x)\dif x = 1.
		\]
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{The Dirac Delta}
	We can move the Dirac delta to any point $x_{0}$ by subtracting $x_{0}$ from its argument:
	\begin{figure}[H]
		\centering
		\begin{tikzpicture}
			\begin{axis}[
					Axis Style,
					width=8cm, height=4cm,
					xmin=-2.5, xmax=2.5,
					ymin=0, ymax=1.5,
					ticks=both,
					axis y line=none,
					xtick={-2,...,2},
					arrow/.style={vector, const plot, no marks, very thick},
					circle/.style={thick, fill=white, very thick, only marks, mark=*},
				]
				\tikzset{delta/.style={above, font=\tiny}}

				\addplot[arrow, col1] coordinates {(-2,0) (-2,1)} node [delta] {$\delta\left(x+2\right)$};
				\addplot[col1, circle, samples at={-2}] {0};

				\addplot[arrow, col2] coordinates {(-0.5,0) (-0.5,1)} node [delta] {$\delta\left(x+0.5\right)$};
				\addplot[col2, circle, samples at={-0.5}] {0};

				\addplot[arrow, col3] coordinates {(0.707,0) (0.707,1)} node [delta] {$\delta\left(\frac{\sqrt{2}}{2}\right)$};
				\addplot[col3, circle, samples at={0.707}] {0};

				\addplot[arrow, col4] coordinates {(1.571,0) (1.571,1)} node [delta] {$\delta\left(x-\frac{\pi}{2}\right)$};
				\addplot[col4, circle, samples at={1.571}] {0};
			\end{axis}
		\end{tikzpicture}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Spanning a Function using Dirac Deltas}
	Recall that a vector $\vec{v}$ cab be represented in any basis $b={\vec{b}_{1}, \vec{b}_{2}, \dots, \vec{b}_{n}}$ as
	\begin{align*}
		\vec{v} &= \alpha_{1}\vec{b}_{1} + \alpha_{2}\vec{b}_{2} + \cdots + \alpha_{n}\vec{b}_{n}\\
		&= \sum\limits_{i=1}^{n}\alpha_{i}\vec{b}_{i}.
	\end{align*}

	Similarily, we can use a set of Dirac deltas to span all the functions $\freal$ using integration.
\end{frame}

\begin{frame}
	\frametitle{Inner Product}
	In chapter 2 we showed that the inner product (dot product) of two vectors $\vec{u}, \vec{v}$ in the standard basis is
	\begin{align*}
		\vec{u}\cdot\vec{v} &= u_{1}v_{1} + u_{2}v_{2} + \cdots + u_{n}v_{n}\\
		&= \sum\limits_{i=1}^{n}\vec{u}_{i}\vec{v}_{i}.
	\end{align*}

	Similarily, we can define the inner product of two functions $f,g\in\cinf$ using the dirac deltas in $[a,b]$ as
	\[
		\bk{f}{g}_{[a,b]} = \int\limits_{a}^{b}\!f(x)g(x)\dif x.
	\]
\end{frame}

\begin{frame}
	\frametitle{Inner Product}
	\begin{presentation_example}
		The dot product of $f(x)=x$ and $g(x)=e^{-x}$ on $[0,1]$ is
		\begin{align*}
			\bk{f}{g}_{[0,1]} &= \int\limits_{0}^{1}\!f(x)g(x)\dif x\\
			&= \int\limits_{0}^{1}\!xe^{-x}\dif x\\
			&= -e^{-x}\left( x+1 \right)\Biggr|_{0}^{1}\\
			&= -\frac{2}{e}+1.\\
			&\approx 0.2642.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Orthogonal Functions}
	Using the inner product we can define the idea of \textbf{orthogonal functions}:
	\begin{presentation_definition}
		Two non-zero functions $\real{f,g}$ are said to be \textbf{orthogonal} to each other on an interval $[a,b]$ if on that interval
		\[
			\bk{f}{g}_{[a,b]}=0.
		\]
	\end{presentation_definition}
\end{frame}

\begin{frame}
	\frametitle{Orthogonal Functions}
	\begin{presentation_example}
		The functions $f(x)=x$ and $g(x)=x^{2}$ are orthogonal over the entire real line, since
		\[
			\bk{f}{g}_{\mathbb{R}} = \sqrt{\int\limits_{-\infty}^{\infty}\! x\cdot x^{2}\dif x} = \sqrt{\int\limits_{-\infty}^{\infty}\! x^{3}\dif x} = 0.
		\]
		(since $x^{3}$ is an antisymmetric function)
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Norm}
	In $\Rs{n}$ we can use the inner product to define the norm of a vector $\vec{v}$ as
	\[
		\norm{\vec{v}} = \sqrt{v_{1}^{2} + v_{2}^{2} + \cdots + v_{n}^{2}} = \sqrt{\vec{v}\cdot\vec{v}}.
	\]

	Similarily, we can define the norm of a function $f\in\cinf$ on the interval $[a,b]$ as
	\[
		\norm{f}_{[a,b]} = \sqrt{\bk{f}{f}}_{[a,b]} = \sqrt{\int\limits_{a}^{b}\!f(x)f(x)\dif x} = \sqrt{\int\limits_{a}^{b}\!f^{2}(x)\dif x}.
	\]
\end{frame}

\begin{frame}
	\frametitle{Norm}
	\begin{presentation_example}
		The norm of $f(x)=e^{-\frac{x^{2}}{2}}$ over $\mathbb{R}$ is
		\begin{align*}
			\norm{f}_{\mathbb{R}} &= \sqrt{\int\limits_{-\infty}^{\infty}\! e^{-\frac{x^{2}}{2}}\cdot e^{-\frac{x^{2}}{2}}\dif x}\\
			&= \sqrt{\int\limits_{-\infty}^{\infty}\! e^{-\cancel{2}\frac{x^{2}}{\cancel{2}}}\dif x}\\
			&= \pi^{\frac{1}{4}}\\
			&\approx 1.33134.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Distance Between Functions}
	In $\Rs{n}$ we define the distance between two vectors $\vec{u}, \vec{v}$ using the difference vector $\vec{u}-\vec{v}$:
	\[
		\dist\left( \vec{u}, \vec{v} \right) = \norm{\vec{u}-\vec{v}}.
	\]
	\begin{presentation_example}
		The distance between the vectors $\vec{u}=\colvec{2}{1}{-3}$ and $\vec{v}=\colvec{2}{0}{2}$ is
		\begin{align*}
			\dist\left(\vec{u},\vec{v}\right) &= \norm{\colvec{2}{1}{-3}-\colvec{2}{0}{2}} = \norm{\colvec{2}{1}{-5}}\\
			&= \sqrt{1^{2}+(-5)^{2}} = \sqrt{26}.
		\end{align*}
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Distance Between Functions}
	Similarily, we define the distance between the functions $\real{f,g}$ on the interval $[a,b]$ as
	\[
		\dist_{\left[ a,b \right]}(f,g) = \norm{f-g}_{\left[ a,b \right]} = \sqrt{\int\limits_{a}^{b}\!\left( f(x)-g(x) \right)^{2}\dif x}.
	\]

	\begin{presentation_example}
		The distance between $x^{2}$ and $\sin(x)$ on the interval $\left[ 0,\pi \right]$ is
		\[
			\dist_{[0,\pi]}\left(x^{2},\sin(x)\right) = \sqrt{\int\limits_{0}^{\pi}\left(x^{2}-\sin(x)\right)^{2}\dif x} \approx 7.14391521.
		\]
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Square-integrable Functions}
	\begin{presentation_definition}
		A \textbf{square-integrable function} $\freal$ is a function for which
		\[
			\int\limits_{-\infty}^{\infty}\! \left| f(x) \right|^{2} \dif x \in \mathbb{R}.
		\]
	\end{presentation_definition}

	\begin{presentation_example}
		The function $f(x)=e^{-x}\sin(x)$ is square-integrable, since
		\[
			\int\limits_{-\infty}^{\infty}\left|e^{-x}\sin(x)\right|^{2}\dif x \approx 0.098\in\mathbb{R}.
		\]
	\end{presentation_example}
\end{frame}

\begin{frame}
	\frametitle{Square-integrable Functions}
	\begin{presentation_note}
		We denote the set of all square-integrable functions as $L^{2}$.
	\end{presentation_note}
\end{frame}

\begin{frame}
	\frametitle{ }
\end{frame}

%\begin{frame}
%	\frametitle{The Dirac Delta}
%	The Dirac delta has a special property: multiplying $\delta(x-x_{0})$ by a smooth function $g(x)$ and then integrating over $\mathbb{R}$ gives the value of $g$ at $x=x_{0}$:
%	\[
%		\int\limits_{-\infty}^{\infty}\delta(x-x_{0})\cdot g(x)\dif x = g(x_{0}).
%	\]
%
%	\begin{presentation_example}
%		\[
%			\int\limits_{-\infty}^{\infty}\delta\left(x-\frac{\pi}{2}\right)\sin(x)\dif x = \sin\left( \frac{\pi}{2} \right) = 1.
%		\]
%	\end{presentation_example}
%\end{frame}
