lectures_folder="lecture_notes"
CHAPTERS = blank ch01_intro ch02_vectors ch03_linear_trans ch04_matrices ch05_linear_systems ch06_eigenvectors ch07_uses ch08_sequences ch09_function_limits ch10_derivatives ch11_integrals ch12_general

$(eval CHPTRNM=$(shell echo $$(($(NUM)+1))))
CHAPTER = $(word $(CHPTRNM), $(CHAPTERS))

do:
	rm -rf pythontex-files-* *.pytxcode
	pdflatex -shell-escape lectures
	biber lectures
	pythontex lectures.pytxcode
	pdflatex -shell-escape lectures

quick:
	pdflatex -shell-escape lectures

presentation:
	pdflatex -shell-escape --jobname '$(CHAPTER)' "\def\chnum{$(NUM)} \input{presentations}"
	mv "$(CHAPTER).pdf" presentation_chapters
	#rm -f $(CHAPTER).*
	#rm -f ../$(CHAPTER).*

withpython:
	rm -rf pythontex-files-* *.pytxcode
	pdflatex -shell-escape --jobname '$(CHAPTER)' "\def\chnum{$(NUM)} \input{presentations}"
	pythontex "$(CHAPTER).pytxcode"
	pdflatex -shell-escape --jobname '$(CHAPTER)' "\def\chnum{$(NUM)} \input{presentations}"
	mv "$(CHAPTER).pdf" presentation_chapters

fullpresent:
	rm -rf pythontex-files-* *.pytxcode
	pdflatex -shell-escape "\def\full{1} \input{presentations}"
	biber lectures
	pythontex presentations.pytxcode
	pdflatex -shell-escape presentations
