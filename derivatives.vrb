\frametitle{Binomial Coefficients}
	\begin{presentation_note}
		The binomial coefficient $\binom{n}{k}$ equals the $k$-th element on the $n$-th of Pascal's triangle.
		\begin{figure}[H]
			\centering
			\begin{tikzpicture}[
					declare function={binom(\n,\m)=\n!+\k!;}
				]
				\colorlet{True}{col1!75}
				\colorlet{False}{col2!75}
				\colorlet{Plus}{col4!85!black}
				\tikzset{
					box/.style={
						minimum height=5mm,
						inner sep=.4mm,
						outer sep=0mm,
						text width=6mm,
						text centered,
						font=\small\bfseries\sffamily,
						text=#1!50!black,
						draw=#1,
						line width=.25mm,
						top color=#1!5,
						bottom color=#1!40,
						shading angle=0,
						rounded corners=2.3mm,
						drop shadow={fill=#1!40!gray,fill opacity=.8},
						rotate=0,
					},
					link/.style={
						draw=#1,
						-stealth,
						line width=.3mm,
					},
					plus/.style={text=Plus,font=\footnotesize\bfseries\sffamily},
				}
	\begin{pycode}
from scipy.special import binom
#with open('test.out', 'w') as f:
N = 5
for n in range(N+1):
	for k in range(n+1):
		bin = int(binom(n, k))
		par = bin%2 == 0
		cmd = '\\node[box={}] (p-{}-{}) at ({},-{}) {{${}$}};'.format(par, n, k, k-n/2, n, bin)
		print(cmd)
		#f.write('{}\n'.format(cmd))

for n in range(N):
	for k in range(n):
		bin1 = int(binom(n, k))
		bin2 = int(binom(n, k+1))
		par1 = bin1%2 == 0
		par2 = bin2%2 == 0
		link1 = '\draw[link={}] (p-{}-{}) -- (p-{}-{});'.format(par1, n, k, n+1, k+1)
		link2 = '\draw[link={}] (p-{}-{}) -- (p-{}-{});'.format(par2, n, k+1, n+1, k+1)
		plus = '\\node[plus,above=.5mm of p-{}-{}]{{$\\bm{{+}}$}};'.format(n+1, k+1)
		print(link1)
		print(link2)
		print(plus)
		#f.write('{}\n'.format(link1))
		#f.write('{}\n'.format(link2))
		#f.write('{}\n'.format(plus))

print('\\node[left of=p-{}-0, xshift=-5mm] (nlbl-{}) {{$n={}$}};'.format(N, N, N))
for n in reversed(range(N)):
	print('\\node[above of=nlbl-{}] (nlbl-{}) {{$n={}$}};'.format(n+1, n, n))
	\end{pycode}
			\end{tikzpicture}
		\end{figure}
	\end{presentation_note}
