\rhead{Integrals}
\begin{enumerate}
	\item Find the indefinite integrals (antiderivative) of the following functions:
		\begin{enumerate}
			\item $-7x^{5}-4x^{3}+2x-5$.
			\if\withsol1{
				\begin{answer}
					We integrate each term separately using
					\begin{equation}
						\int\!x^{n}\dif x = \frac{x^{n-1}}{n-1} + C,
						\label{eq:powerint}
					\end{equation}
					and collecting all the integration constants together:
					\begin{align*}
						\int\!\left(-7x^{5}-4x^{3}+2x-5\right)\dif x &= \int\!\left(-7x^{5}\right)\dif x + \int\!\left(-4x^{3}\right)\dif x + \int\!2x\dif x + \int\!5\dif x\\
						&= -7\int\!x^{5}\dif x -4\int\!x^{3}\dif x + 2\int\!x\dif x + 5\int\!\dif x\\
						&= -7\frac{x^{6}}{6} - \cancel{4}\frac{x^{4}}{\cancel{4}} + \cancel{2}\frac{x^{2}}{\cancel{2}} + 5x + C\\
						&= -\frac{7}{6}x^{6} - x^{4} + x^{2} + 5x + C.
					\end{align*}
				\end{answer}
			}\fi
			
			\item $\sin(2x)$.
			\if\withsol1{
				\begin{answer}
					We know that $\od{}{x}\cos(x)=-\sin(x)$. Therefore, $\od{}{x}\left[ -\cos(2x) \right] = 2\sin(2x)$. By dividing this function by $2$ we get the function we wanted to integrate. Therefore,
					\[
						\int\!\sin(2x)\dif x = -\frac{1}{2}\cos(2x) + C.
					\]
				\end{answer}
			}\fi

			\item $2e^{-x}$.
			\if\withsol1{
				\begin{answer}
					We know that
					\[
						\int\!e^{-x}\dif x = -e^{-x},
					\]
					and therefore
					\[
						\int\!\left( 2e^{-x} \right)\dif x = 2\int\!e^{-x}\dif x = -2e^{-x} + C.
					\]
				\end{answer}
			}\fi
		\end{enumerate}

	\item Using \textbf{integration by parts}, find
		\[
			\int\!x\sqrt{x+1}\dif x.
		\]
		\if\withsol1{
			\begin{answer}
				Let us define
				\[
					f(x)=x,\ g'(x)=\sqrt{x+1}.
				\]
				This gives (leaving the integration constant for now):
				\[
					f'(x)=1,\ g(x)=\frac{2}{3}\sqrt{\left( x+1 \right)^{3}}=\frac{2}{3}\left( x+1 \right)^{\frac{3}{2}},
				\]
				since $\sqrt{x+1}=\left( x+1 \right)^{\frac{1}{2}}$, and following equation \ref{eq:powerint}. Subtituting $f,f',g$ and $g'$ into
				\[
					\int\!f(x)g'(x)\dif x = f(x)g(x) - \int\!f'(x)g(x)\dif x,
				\]
				yields
				\begin{align*}
					\int\!x\sqrt{x+1}\dif x &= \frac{2}{3}x\left(x+1\right)^{\frac{3}{2}} - \frac{2}{3}\int\!\left(x+1\right)^{\frac{3}{2}}\dif x\\
					&= \frac{2}{3}x\left(x+1\right)^{\frac{3}{2}} - \frac{2}{3}\left[ \frac{2}{5}\left( x+1 \right)^\frac{5}{2} \right] + C\\
					&= \frac{2}{3}x\left(x+1\right)^{\frac{3}{2}} - \frac{4}{15}\left( x+1 \right)^{\frac{5}{2}} + C.
				\end{align*}
			\end{answer}
		}\fi

	\item Using \textbf{integration by substitution}, find
		\[
			\int\!\frac{x}{\sqrt{1-4x^{2}}}\dif x.
		\]
		\if\withsol1{
			\begin{answer}
				We define
				\[
					u = 1-4x^{2},
				\]
				which means
				\[
					\od{u}{x}=-8x,
				\]
				i.e.
				\[
					x\dif x = -\frac{1}{8}\dif u.
				\]

				We re-write the integral using these substitutions and solve it:
				\begin{align*}
					\int\!\frac{x}{\sqrt{1-4x^{2}}}\dif x &= -\frac{1}{8}\int\!\frac{1}{\sqrt{u}}\dif u\\
					&= -\frac{1}{8}\int\!u^{-\frac{1}{2}}\dif u\\
					&= -\frac{1}{8}\left[ \frac{u^\frac{1}{2}}{\frac{1}{2}} \right] + C\\
					&= -\frac{1}{4}u^{\frac{1}{2}} + C\\
					&= -\frac{1}{4}\sqrt{u} + C\\
					&= -\frac{1}{4}\sqrt{1-4x^{2}} + C.
				\end{align*}
			\end{answer}
		}\fi

	\item 
		\begin{enumerate}
			\item Calculate the area between the function $f(x)=x^{3}-4x$ and the $x$-axis, for $x<2$ and above $y=0$.
	\if\withsol1{
		\begin{answer}
			First, let us calculate the antiderivative of $f$:
			\[
				\int\!f(x)\dif x = \int\!\left( x^{3}-4x \right)\dif x = \frac{1}{4}x^{4}-2x^{2} + C.
			\]
			We then look at the function (the area to be calculated is in green):
			\begin{figure}[H]
				\centering
				\begin{tikzpicture}
					\begin{axis}[
							Axis Style,
							width=10cm, height=12cm,
							xmin=-3, xmax=3,
							ymin=-4, ymax=4,
							declare function={f(\x)=\x^3-4*\x;},
						]
						\addplot[name path=fx, function, col1] {f(x)};
						\path[name path=xaxis] (axis cs:-3,0) -- (axis cs:3,0);
						\addplot[function, col1] {f(x)};
						\addplot[col3, opacity=0.3] fill between [of=fx and xaxis, soft clip={domain=-2:0}];
					\end{axis}
				\end{tikzpicture}
			\end{figure}

			We therefore need to calculate the following:
			\[
				\int\limits_{-2}^{0}\!\left( x^{3}-4x \right)\dif x.
			\]

			Since we already calculated $\displaystyle\int\! \left( x^{3}-4x \right)\dif x$, this is easy:
			\begin{align*}
				\int\limits_{-2}^{0}\!\left( x^{3}-4x \right)\dif x &= \frac{1}{4}x^{4}-2x^{2}\Biggr|_{-2}^{0}\\
				&= 0 - 0 - \left( \frac{1}{4}(-2)^{4}-2(-2)^{2} \right)\\
				&= -(-4 + 8)\\
				&= 4.
			\end{align*}
		\end{answer}
	}\fi
		\end{enumerate}

	\item Calculate $\displaystyle\int_{-\frac{\pi}{2}}^{\frac{\pi}{2}}\!f(x)\dif x$.
	\if\withsol1{
		\begin{answer}
			Of course one can directly calculate
			\[
				\frac{1}{4}x^{4}-4x^{2}\Biggr|_{\frac{\pi}{2}}^{\frac{\pi}{2}}.
			\]
			However, it is much easier to notice that
			\[
				f(-a) = -a^{3}+4a = -(a^{3}-4a) = -f(a),
			\]
			i.e. $f$ is \textbf{antisymmetric}. Therefore
			\[
				\int\limits_{-\frac{\pi}{2}}^{\frac{\pi}{2}}\!f(x)\dif x = 0.
			\]
		\end{answer}
	}\fi
\end{enumerate}
