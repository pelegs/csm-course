#!/usr/bin/env python3
# -*- coding: iso-8859-15 -*-

import re
from fractions import Fraction

def num_format(s):
    # Fractions
    # If a number not integer, replace it with
    # a correctly formatted fraction
    fracs = re.findall('-*\d+\.\d*', s)
    for frac in fracs:
        frmt = Fraction(float(frac)).limit_denominator()
        frac = frac.replace('.','\\.')
        num, den = abs(frmt.numerator), frmt.denominator
        neg = frmt.numerator < 0
        substr = '\\\\frac{{{}}}{{{}}}'.format(num, den)
        if neg:
            substr = '-' + substr
        s = re.sub(frac, substr, s)
        if den == 1:
            s = re.sub('\\\\frac{(\d+)}{(\d+)}', '\g<1>', s)
    # Deal with minus signs
    s = s.replace('+-','-')
    return s

def series_example(a, r, n_max=7):
    exs = num_format('$' + '+'.join([str(a*r**n) for n in range(n_max)]) + '+\\cdots$')
    print('&'.join(['${}$'.format(num_format(str(a))), '${}$'.format(num_format(str(r))), exs]))
