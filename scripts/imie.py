#!/usr/bin/env python3
# -*- coding: iso-8859-15 -*-

import numpy as np
from fractions import Fraction


def gen_IMIE(dim=3, min=-3, max=3):
    while True:
        P = np.random.randint(min, max, (dim,dim))
        if np.linalg.det(P) != 0:
            Pinv = np.linalg.inv(P)
            d = np.linalg.det(P)
            x = np.random.randint(5)
            ds = np.array([x+n*d for n in np.random.randint(-3, 3, dim)])
            D = np.diag(ds)
            A = np.dot(P, np.dot(D, Pinv))
            if np.linalg.det(A) != 0:
                e, v = np.linalg.eig(A)
                if int(e[0])==e[0] and int(e[1])==e[1]:
                    return A.astype(int), ds.astype(int), P.astype(int)

def gen_N_IMIEs(N=10, dim=3, min=-3, max=3):
    A = np.zeros((N,dim,dim))
    e = np.zeros((N,dim))
    v = np.zeros((N,dim,dim))
    for i in range(N):
        A[i], e[i], v[i] = gen_IMIE(dim, min, max)
    return A, e, v


if __name__ == '__main__':
    A, e, v = gen_N_IMIEs(dim=2, min=-5, max=5)
    print(e)
