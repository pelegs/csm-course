#!/usr/bin/env python3.6
# -*- coding: iso-8859-15 -*-

from sys import argv

def identity_matrix(n):
    print('\\begin{pmatrix}')
    for i in range(n):
        row = [0] * n
        row[i] = 1
        print('&'.join(map(str, row)), '\\\\')
    print('\\end{pmatrix}')

def colvec(v):
    dim = v.shape[0]
    s = '\\colvec{{{}}}'.format(dim)
    for x in v:
        s += '{{{}}}'.format(x)
    return s

def colvec_int(v):
    return colvec(v.astype(int))

def pmatrix(m):
    s = '\\begin{pmatrix}'
    for row in m:
        s += '&'.join(map(str, row)) + '\\\\'
    s += '\\end{pmatrix}'
    return s

def pmatrix_int(m):
    mint = m.astype(int)
    return pmatrix(mint)


if __name__ == '__main__':
    identity_matrix(int(argv[1]))
